# Changelog
Ce fichier contient les modifications techniques du module.

## [2.15.1.0] - 2020-06-05
### Evolution
Diminuer le temps d'exécution d'un script javascript

### Ajout

- __Properties__ 

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | scripts.execution.timeout | Script exexution time to live (in seconds) | 900 |

## [2.14.5.4] - 2020-05-14
### Evolution
Limiter le nombre de dossiers dans la pile AWAIT de Markov

### Ajout

- __Properties__ 

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | markov.instance.max.await.messages | Nombre maximum de dossiers en cours de traitement | 10 |

## [2.14.5.2] - 2020-05-14
### Evolution
Intégration de Togglz

## [2.14.5.0] - 2020-05-05
### Evolution
Mise en place de la gestion des transaction par Spring

## [2.14.3.0] - 2020-04-06
### Evolution
Forcer le canal de réception d'une autorité dans les transitions

## [2.13.1.3] - 2019-12-06
### Evolution

Ajouter un clic lorsqu'un dossier est transmis à une autorité

### Ajout

- __Properties__ 

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| engine.properties | ws.clicker.url | URL privée de clicker | ${CLICKER_WS_INT}/api |

## [2.13.1.1] - 2019-12-05
### Evolution

Modification des transitions d'envoie de dossiers vers EDDIE (revue du nommage des fichiers envoyés aux autorités) 

## [2.12.5.0] - 2019-11-20
### Evolution

Ajout de métadonnées lors du dépôt d'un dossier dans STORAGE :
* Auteur du dossier
* Titre
* Description


## [2.11.3.0] - 2019-05-23

- Projet : [Markov](https://tools.projet-ge.fr/gitlab/minecraft/ge-markov)

###  Ajout

### Modification

### Suppression

## Liens

[2.11.3.0](https://tools.projet-ge.fr/gitlab/minecraft/ge-markov/tags/ge-markov-2.11.3.0)


## [2.11.2.5] - 2019-05-10

- Projet : [Markov](https://tools.projet-ge.fr/gitlab/minecraft/ge-markov)

###  Ajout

### Modification
- Traduction des messages postés dans TRACKER

### Suppression

## Liens

[2.11.2.5](https://tools.projet-ge.fr/gitlab/minecraft/ge-markov/tags/ge-markov-2.11.2.5)


## [2.11.1.3] - 2019-04-30

- Projet : [Markov](https://tools.projet-ge.fr/gitlab/minecraft/ge-markov)

###  Ajout
- Nouveau fichier _Changelog.md_

### Modification

### Suppression

## Liens

[2.11.1.3](https://tools.projet-ge.fr/gitlab/minecraft/ge-markov/tags/ge-markov-2.11.1.3)