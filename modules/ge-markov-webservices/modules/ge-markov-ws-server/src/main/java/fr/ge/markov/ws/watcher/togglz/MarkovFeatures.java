package fr.ge.markov.ws.watcher.togglz;

import org.togglz.core.Feature;
import org.togglz.core.annotation.EnabledByDefault;
import org.togglz.core.annotation.Label;
import org.togglz.core.context.FeatureContext;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.repository.FeatureState;

/**
 * The Togglz Markov features;
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum MarkovFeatures implements Feature {

    @Label("Active l'upload des dossiers dans le BDD Markov")
    @EnabledByDefault
    SCAN,

    @Label("Active le thread d'envoi de dossiers dans la pile SUCCESS de STORAGE")
    @EnabledByDefault
    SUCCESS,

    @Label("Active le thread d'envoi de dossiers dans la pile ERROR de STORAGE")
    @EnabledByDefault
    ERROR,

    @Label("Active le watcher d'exécution des dossiers en cours de traitement dans Markov")
    @EnabledByDefault
    INPROGRESS,

    @Label("Active le watcher des dossiers en attente de traitement dans Markov")
    @EnabledByDefault
    AWAIT,

    @Label("Active le thread d'envoi de dossiers dans la pile INFERNO de STORAGE")
    @EnabledByDefault
    INFERNO;

    public boolean isActive() {
        return FeatureContext.getFeatureManager().isActive(this);
    }

    public static void update(final String featureName, final boolean state) {
        final FeatureManager featureManager = FeatureContext.getFeatureManager();
        featureManager.getFeatures() //
                .stream() //
                .filter(f -> f.name().equals(featureName.toUpperCase())) //
                .findFirst() //
                .map(f -> {
                    final FeatureState featureState = featureManager.getFeatureState(f);
                    featureState.setEnabled(state);
                    featureManager.setFeatureState(featureState);
                    return f;
                }) //
                .orElse(null);
    }

    public static FeatureState getFeature(final String featureName) {
        final FeatureManager featureManager = FeatureContext.getFeatureManager();
        return featureManager.getFeatures().stream().filter(f -> f.name().equals(featureName.toUpperCase())).findFirst().map(f -> featureManager.getFeatureState(f)).orElse(null);
    }

    public static boolean isActive(final String featureName) {
        final FeatureManager featureManager = FeatureContext.getFeatureManager();
        return featureManager.getFeatures().stream().filter(f -> f.name().equals(featureName.toUpperCase())).findFirst().map(f -> featureManager.isActive(f)).orElse(false);
    }
}
