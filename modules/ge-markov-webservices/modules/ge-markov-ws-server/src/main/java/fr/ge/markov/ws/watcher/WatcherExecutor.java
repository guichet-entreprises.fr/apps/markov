/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.watcher;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.ws.watcher.process.IMessageProcess;
import fr.ge.markov.ws.watcher.togglz.MarkovFeatures;

/**
 * Watcher executor.
 *
 * @author jpauchet
 */
public class WatcherExecutor {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(WatcherExecutor.class);

    /** A task executor. */
    private ThreadPoolTaskExecutor taskExecutor;

    /** The watchers to execute. */
    private List<AbstractWatcher> watchers;

    /** Total number of replicas for In Progress queue watcher **/
    private int totalWorkerInProgress;

    /** Thread sleep time for In Progress queue watcher **/
    private int threadSleepWorkerInProgress;

    /** Message processs. */
    private IMessageProcess messageProcess;

    /** Record storage service. */
    private IRecordStorage recordStorage;

    /** Queue message service. */
    private IQueueMessageService messageService;

    /** Prefix worker replica in progress. */
    private String prefixWorkerInProgress;

    /**
     * Sets the task executor.
     *
     * @param taskExecutor
     *            the new task executor
     * @return the watcher executor
     */
    public WatcherExecutor setTaskExecutor(final ThreadPoolTaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
        return this;
    }

    /**
     * Gets the watchers.
     * 
     * @return the watcher executor
     */
    public List<AbstractWatcher> getWatchers() {
        return this.watchers;
    }

    /**
     * Sets the watchers.
     *
     * @param watchers
     *            the new watchers
     * @return the watcher executor
     */
    public WatcherExecutor setWatchers(final List<AbstractWatcher> watchers) {
        this.watchers = watchers;
        return this;
    }

    /**
     * Set the total workers in progress
     * 
     * @param totalWorkerInProgress
     *            the total workers in progress
     * @return the watcher executor
     */
    public WatcherExecutor setTotalWorkerInProgress(final int totalWorkerInProgress) {
        this.totalWorkerInProgress = totalWorkerInProgress;
        return this;
    }

    /**
     * Set the thread workers sleeping time in ms
     * 
     * @param threadSleepWorkerInProgress
     *            the thread workers sleeping time in ms
     * @return the watcher executor
     */
    public WatcherExecutor setThreadSleepWorkerInProgress(final int threadSleepWorkerInProgress) {
        this.threadSleepWorkerInProgress = threadSleepWorkerInProgress;
        return this;
    }

    /**
     * Set the service to handle messages
     * 
     * @param messageProcess
     *            the service to handle messages
     * @return the watcher executor
     */
    public WatcherExecutor setMessageProcess(final IMessageProcess messageProcess) {
        this.messageProcess = messageProcess;
        return this;
    }

    /**
     * Set the service to store records
     * 
     * @param recordStorage
     *            the service to store records
     * @return the watcher executor
     */
    public WatcherExecutor setRecordStorage(final IRecordStorage recordStorage) {
        this.recordStorage = recordStorage;
        return this;
    }

    /**
     * Set the service to handle queues
     * 
     * @param recordStorage
     *            the service to handle queues
     * @return the watcher executor
     */
    public WatcherExecutor setMessageService(final IQueueMessageService messageService) {
        this.messageService = messageService;
        return this;
    }

    /**
     * Set the service to handle queues
     * 
     * @param recordStorage
     *            the service to handle queues
     * @return the watcher executor
     */
    public WatcherExecutor setPrefixWorkerInProgress(final String prefixWorkerInProgress) {
        this.prefixWorkerInProgress = prefixWorkerInProgress;
        return this;
    }

    /**
     * Inits.
     */
    public void init() {
        final List<AbstractWatcher> totalWatchers = new ArrayList<AbstractWatcher>();
        totalWatchers.addAll(this.watchers);

        // -->Create an Queue Watcher for in progress execution
        for (int i = 1; i <= this.totalWorkerInProgress; i++) {
            final String workerReplicaName = this.prefixWorkerInProgress.concat(String.valueOf(i));
            final QueueWatcher queueWatcher = new QueueWatcher();
            queueWatcher.setFeature(MarkovFeatures.INPROGRESS);
            queueWatcher.setThreadSleep(this.threadSleepWorkerInProgress);
            queueWatcher.setWorker(workerReplicaName);
            queueWatcher.setMessageProcess(this.messageProcess);
            queueWatcher.setRecordStorage(this.recordStorage);
            queueWatcher.setMessageService(this.messageService);
            totalWatchers.add(queueWatcher);
            LOGGER.debug("Add replica worker {} to the list of watchers", workerReplicaName);
        }

        for (final AbstractWatcher watcher : totalWatchers) {
            this.taskExecutor.execute(watcher);
        }
    }

}
