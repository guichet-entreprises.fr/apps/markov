/**
 *
 */
package fr.ge.markov.ws.v1.service.impl;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.ws.v1.service.IFormsRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */
@Api
@Path("/v1/forms")
public class FormsRestServiceImpl implements IFormsRestService {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(FormsRestServiceImpl.class);

    /**
     * the IQueueMessageFileMapper
     */
    @Autowired
    private IQueueMessageService queueMessageService;

    /**
     * Quick and Dirty need to refresh. {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Method allowing to put a form for Markov.")
    @ApiImplicitParams({ @ApiImplicitParam(name = "attachmentForms", value = "Here you  add your form to upload for Markov.", required = true, dataType = "java.io.File", paramType = "body") })
    public Response uploadForms(final InputStream attachmentForms, final String uid) {

        LOGGER_FONC.debug("WebService for forms upload");
        if (StringUtils.isEmpty(uid)) {
            LOGGER_FONC.error("the uid of record is empty");
            return Response.serverError().build();
        }

        this.queueMessageService.deleteByUid(uid);
        // Form to byte array conversion.
        final byte[] formsByte;
        try {
            formsByte = IOUtils.toByteArray(attachmentForms);
            this.queueMessageService.add(MessageQueueStatusEnum.AWAIT.toString(), formsByte, uid);
        } catch (final IOException e) {
            LOGGER_FONC.error("Error converting the file to byte array.", e);
            return Response.serverError().build();
        }

        return Response.ok().build();
    }
}
