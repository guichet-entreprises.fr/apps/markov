/**
 *
 */
package fr.ge.markov.ws.v1.service.impl;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.service.IQueueMessageFileService;
import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.ws.v1.bean.ResponseQueueMessageBean;
import fr.ge.markov.ws.v1.service.IQueueMessageRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author $Author:ABSIBISS $
 * @version $Revision: 0 $
 */
@Api
@Path("/v1/queue")
public class QueueMessageRestServiceImpl implements IQueueMessageRestService {

    /**
     * the IQueueMessageFileMapper
     */
    @Autowired
    private IQueueMessageService queueMessageService;

    @Autowired
    private IQueueMessageFileService queueMessageFileService;

    @Autowired
    private Engine engine;

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download a queued record.")
    public Response download(@ApiParam("Record UID") final String uid) {

        final QueueMessageBean queueMessage = this.queueMessageService.findByUid(uid);

        if (null == queueMessage) {
            return Response.noContent().build();
        }

        final QueueMessageFileBean queueFile = this.queueMessageFileService.findByIdMessage(queueMessage.getId());

        byte[] asBytes = null;
        if (null != queueFile) {
            asBytes = queueFile.getContent();
        }

        return Response.ok(asBytes) //
                .header(HttpHeaders.CONTENT_TYPE, "application/zip") //
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + queueMessage.getUid() + ".zip") //
                .lastModified(queueMessage.getUpdated()) //
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download a queued record.")
    public Response download(@ApiParam("Queue message ID") final long id) {
        final QueueMessageBean queueMessage = this.queueMessageService.findById(id);

        if (null == queueMessage) {
            return Response.noContent().build();
        }

        final QueueMessageFileBean queueFile = this.queueMessageFileService.findByIdMessage(id);

        byte[] asBytes = null;
        if (null != queueFile) {
            asBytes = queueFile.getContent();
        }

        return Response.ok(asBytes) //
                .header(HttpHeaders.CONTENT_TYPE, "application/zip") //
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + queueMessage.getUid() + ".zip") //
                .lastModified(queueMessage.getUpdated()) //
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for queue messages", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SearchResult<ResponseQueueMessageBean> search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders) {

        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders);
        return this.queueMessageService.search(searchQuery, ResponseQueueMessageBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Delete a queue message")
    public Response delete(@ApiParam("Queue message ID") final long id) {
        final long deleted = this.queueMessageService.deleteById(id);

        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Create a new queue message from record file")
    public Response upload(@ApiParam("Queue code") final String queueCode, //
            @ApiParam("[true] to remove previous message for same record UID, [false] or not specified otherwise") final boolean overwritePreviousOne, //
            final Attachment attachment) {

        final byte[] resourceAsBytes = attachment.getObject() instanceof byte[] ? (byte[]) attachment.getObject() : attachment.getObject(byte[].class);

        if (ArrayUtils.isEmpty(resourceAsBytes)) {
            return Response.serverError().entity("Record content is empty").build();
        }

        final SpecificationLoader record = this.engine.loader(new ZipProvider(resourceAsBytes));

        if (null == record) {
            return Response.serverError().entity("Unable to load record file").build();
        }

        if (null == record.description()) {
            return Response.serverError().entity("Unable to load record metadata").build();
        }

        final String uid = record.description().getRecordUid();
        if (overwritePreviousOne) {
            this.queueMessageService.deleteByUid(uid);
            this.queueMessageService.add(queueCode, resourceAsBytes, uid);
        } else {
            this.queueMessageService.add(queueCode, resourceAsBytes, uid);
        }

        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Move a message to a new queue")
    public Response move(@ApiParam("Queue message ID") final long id, @ApiParam("Destination queue code") final String dstQueueCode) {

        final Date now = new Date();
        final long moved = this.queueMessageService.move(id, dstQueueCode, now);
        if (moved > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();
    }

}
