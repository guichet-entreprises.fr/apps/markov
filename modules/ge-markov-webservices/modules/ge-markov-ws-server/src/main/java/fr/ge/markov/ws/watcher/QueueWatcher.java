/**
 *
 */
package fr.ge.markov.ws.watcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.ws.watcher.process.IMessageProcess;
import fr.ge.markov.ws.watcher.process.MessageProcessResult;
import fr.ge.markov.ws.watcher.process.MessageProcessResult.Result;

/**
 * Queue watcher.
 *
 * @author bsadil
 *
 */
public class QueueWatcher extends AbstractWatcher {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueWatcher.class);

    /** The worker instance of markov. */
    private String worker;

    /** The thread sleep. */
    private long threadSleep = 3000;

    /** Message service. */
    @Autowired
    private IQueueMessageService messageService;

    /** Message processs. */
    private IMessageProcess messageProcess;

    /** Watched status. */
    private MessageQueueStatusEnum watchedStatus = MessageQueueStatusEnum.AWAIT;

    /** Record storage service. */
    @Autowired
    private IRecordStorage recordStorage;

    /**
     * Sets the worker.
     *
     * @param worker
     *            the worker to set
     * @return the queue watcher
     */
    public QueueWatcher setWorker(final String worker) {
        this.worker = worker;
        return this;
    }

    /**
     * Sets the message service.
     *
     * @param messageService
     *            the messageService to set
     * @return the queue watcher
     */
    public QueueWatcher setMessageService(final IQueueMessageService messageService) {
        this.messageService = messageService;
        return this;
    }

    /**
     * Sets the message process.
     *
     * @param messageProcess
     *            the message process
     * @return the queue watcher
     */
    public QueueWatcher setMessageProcess(final IMessageProcess messageProcess) {
        this.messageProcess = messageProcess;
        return this;
    }

    /**
     * Sets the thread sleep.
     *
     * @param threadSleep
     *            the threadSleep to set
     * @return the queue watcher
     */
    public QueueWatcher setThreadSleep(final long threadSleep) {
        this.threadSleep = threadSleep;
        return this;
    }

    /**
     * Sets the watched status.
     *
     * @param watchedStatus
     *            the watched status
     * @return the queue watcher
     */
    public QueueWatcher setWatchedStatus(final MessageQueueStatusEnum watchedStatus) {
        this.watchedStatus = watchedStatus;
        return this;
    }

    /**
     * Mutateur sur l'attribut {@link #recordStorage}.
     *
     * @param recordStorage
     *            la nouvelle valeur de l'attribut recordStorage
     */
    public QueueWatcher setRecordStorage(final IRecordStorage recordStorage) {
        this.recordStorage = recordStorage;
        return this;
    }

    /**
     * Watches.
     */
    public void watch() {
        final QueueMessageBean queueMessage = this.messageService.next(this.worker, this.watchedStatus);

        this.execute(queueMessage);

        /*
         * Wait between message processing
         */
        try {
            Thread.sleep(this.threadSleep);
        } catch (final InterruptedException e) {
            // -->Do nothing here
        }
    }

    private void execute(final QueueMessageBean queueMessage) {
        if (null == queueMessage) {
            LOGGER.error("The worker #{} cannot process any message !!", this.worker);
            return;
        }

      //  LOGGER.info("Process awaiting message #{} and worker #{}", queueMessage.getId(), queueMessage.getWorker());

        try {
            final QueueMessageFileBean queueMessageFile = this.messageService.findMessageContent(queueMessage.getId());

            final MessageProcessResult result = this.messageProcess.accept( //
                    queueMessage.getUid(), //
                    queueMessageFile.getContent(), //
                    resourceAsBytes -> this.updateQueueMessage(resourceAsBytes, queueMessage, queueMessageFile) //
            );

            if (Result.SUCCESS == result.getStatus()) {
                this.releaseQueueMessage(result, MessageQueueStatusEnum.SUCCESS, queueMessage, queueMessageFile);
            } else if (Result.INFERNO == result.getStatus()) {
                this.releaseQueueMessage(result, MessageQueueStatusEnum.INFERNO, queueMessage, queueMessageFile);
            } else if (Result.ERROR == result.getStatus()) {
                this.releaseQueueMessage(result, MessageQueueStatusEnum.ERROR, queueMessage, queueMessageFile);
            } else if (Result.RETRY == result.getStatus()) {
                this.releaseQueueMessage(result, null, queueMessage, queueMessageFile);
            }
        } catch (final Throwable ex) {
            LOGGER.warn("Error occured while processing message #{}", queueMessage.getId(), ex);
            queueMessage.setQueueCode(MessageQueueStatusEnum.ERROR.toString());
            this.messageService.release(queueMessage, null);
        }
    }

    /**
     * Update queue message.
     *
     * @param result
     *            the result
     * @param target
     *            the target
     * @param queueMessage
     *            the queue message
     * @param queueMessageFile
     *            the queue message file
     */
    private void releaseQueueMessage(final MessageProcessResult result, final MessageQueueStatusEnum target, final QueueMessageBean queueMessage, final QueueMessageFileBean queueMessageFile) {
        if (null != target) {
            if (MessageQueueStatusEnum.SUCCESS == target && target.toString().equals(queueMessage.getQueueCode()) //
                    || MessageQueueStatusEnum.SUCCESS == target && MessageQueueStatusEnum.INFERNO.toString().equals(queueMessage.getQueueCode()) //
                    || MessageQueueStatusEnum.SUCCESS == target && MessageQueueStatusEnum.ERROR.toString().equals(queueMessage.getQueueCode())) {
                this.messageService.deleteById(queueMessage.getId());
                return;
            } else {
                queueMessage.setQueueCode(target.toString());
            }
        }
        if (null == result.getResource()) {
            this.messageService.release(queueMessage, null);
        } else {
            queueMessageFile.setContent(result.getResource());
            this.messageService.release(queueMessage, queueMessageFile);
        }
    }

    private void updateQueueMessage(final byte[] resourceAsBytes, final QueueMessageBean queueMessage, final QueueMessageFileBean queueMessageFile) {
        if (null != resourceAsBytes) {
            queueMessageFile.setContent(resourceAsBytes);
            this.messageService.update(queueMessage, queueMessageFile);
        }
    }
}
