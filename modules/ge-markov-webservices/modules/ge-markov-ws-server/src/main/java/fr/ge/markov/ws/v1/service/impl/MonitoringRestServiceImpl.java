/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.v1.service.impl;

import java.io.File;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.util.FeatureStateStorageWrapper;

import fr.ge.common.utils.service.IHealthCheckService;
import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.QueueMessageSummaryBean;
import fr.ge.markov.ws.v1.service.IMonitoringRestService;
import fr.ge.markov.ws.watcher.togglz.MarkovFeatures;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Christian Cougourdan
 *
 */
@Api
@Path("/v1/monitoring")
public class MonitoringRestServiceImpl implements IMonitoringRestService, IHealthCheckService {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueMessageRestServiceImpl.class);

    @Autowired
    private IQueueMessageService service;

    @Value("${markov.directory.input}")
    private String inputDirectory;

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Monitoring information")
    public Response summary() {
        final QueueMessageSummaryBean bean = this.service.summary();
        if (null == bean) {
            return Response.noContent().build();
        } else {
            return Response.ok(bean).build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FeatureStateStorageWrapper getFeature(final String feature) {
        final FeatureState fs = MarkovFeatures.getFeature(feature);
        if (null == fs) {
            LOGGER.debug("No feature {} found", feature);
            return null;
        }
        return FeatureStateStorageWrapper.wrapperForFeatureState(fs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response updateFeature(final String feature, final boolean state) {
        MarkovFeatures.update(feature, state);
        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer pending() {
        return FileUtils.listFiles(new File(this.inputDirectory), new String[] { "sha1" }, true).size();
    }
}
