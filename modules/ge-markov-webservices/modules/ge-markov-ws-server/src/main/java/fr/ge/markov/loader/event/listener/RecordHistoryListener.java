/**
 *
 */
package fr.ge.markov.loader.event.listener;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.loader.event.MessageEvent;
import fr.ge.common.nash.engine.loader.event.Subscribe;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationHistory;
import fr.ge.common.nash.engine.mapping.form.v1_2.history.HistoryEventElement;

/**
 * The record history listener.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordHistoryListener {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordHistoryListener.class);

    /** Date format. */
    private static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";

    @Subscribe
    public void onMessageEvent(final MessageEvent messageEvent) {
        LOGGER.debug("Trigger history event");

        final Calendar now = Calendar.getInstance();
        final String formattedNow = new SimpleDateFormat(DATE_FORMAT).format(now.getTime());
        final String enhancedMessage = MessageFormat.format("Le dossier a été traité automatiquement. {0}", messageEvent.getMessage());

        final HistoryEventElement historyEventElement = new HistoryEventElement(formattedNow, enhancedMessage);

        final FormSpecificationHistory history = messageEvent.getLoader().history();
        history.getEvents().add(historyEventElement);

        messageEvent.getLoader().save(messageEvent.getLoader().historyPath(), history);
    }

}
