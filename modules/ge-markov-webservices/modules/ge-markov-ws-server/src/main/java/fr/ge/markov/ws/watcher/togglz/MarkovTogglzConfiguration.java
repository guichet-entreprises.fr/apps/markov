/**
 * 
 */
package fr.ge.markov.ws.watcher.togglz;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.togglz.core.Feature;
import org.togglz.core.manager.TogglzConfig;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.repository.jdbc.JDBCStateRepository;
import org.togglz.core.user.FeatureUser;
import org.togglz.core.user.SimpleFeatureUser;
import org.togglz.core.user.UserProvider;

/**
 * The Togglz configuration.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class MarkovTogglzConfiguration implements TogglzConfig {

    /** Le data source. */
    @Autowired
    private DataSource dataSource;

    /**
     * Setter de l'attribut dataSource.
     *
     * @param dataSource
     *            la nouvelle valeur de dataSource
     */
    public MarkovTogglzConfiguration setDataSource(final DataSource dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<? extends Feature> getFeatureClass() {
        return MarkovFeatures.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StateRepository getStateRepository() {
        // return new CachingStateRepository(new
        // JDBCStateRepository(this.dataSource), DUREE_CACHE);
        return new JDBCStateRepository(this.dataSource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserProvider getUserProvider() {
        return new UserProvider() {
            @Override
            public FeatureUser getCurrentUser() {
                return new SimpleFeatureUser("admin", true);
            }
        };
    }

}
