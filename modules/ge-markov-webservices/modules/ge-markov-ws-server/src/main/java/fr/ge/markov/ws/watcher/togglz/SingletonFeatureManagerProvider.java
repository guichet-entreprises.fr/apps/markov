/**
 * 
 */
package fr.ge.markov.ws.watcher.togglz;

import javax.sql.DataSource;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.manager.FeatureManagerBuilder;
import org.togglz.core.spi.FeatureManagerProvider;

/**
 * Togglz feature manager.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component
public class SingletonFeatureManagerProvider implements FeatureManagerProvider {

    /** Le feature manager. */
    private static FeatureManager featureManager;

    /**
     * {@inheritDoc}
     */
    @Override
    public int priority() {
        return 30;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized FeatureManager getFeatureManager() {
        if (null == featureManager) {
            final DataSource dataSource = (DataSource) new ClassPathXmlApplicationContext("classpath:spring/togglz-context.xml").getBean("dataSource");
            featureManager = new FeatureManagerBuilder().togglzConfig(new MarkovTogglzConfiguration().setDataSource(dataSource)).build();

        }
        return featureManager;
    }
}
