/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.watcher.process;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.markov.service.IFormsService;
import fr.ge.markov.ws.watcher.process.MessageProcessResult.Result;

/**
 * Step execute message process.
 *
 * @author Christian Cougourdan
 */
public class StepExecuteMessageProcess implements IMessageProcess {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(StepExecuteMessageProcess.class);

    /** The forms service. */
    @Autowired
    private IFormsService service;

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageProcessResult accept(final String code, final byte[] resourceAsBytes, final Consumer<byte[]> writer) {
        LOGGER.info("begin execute step of Record with uid {}  markov", code);

        final IProvider provider = new ZipProvider(resourceAsBytes);

        Result status;
        try {
            while (this.service.handleStep(provider)) {
                writer.accept(provider.asBytes());
            }

            // the case of last Step
            status = Result.SUCCESS;
        } catch (final ExpressionException ex) {
            LOGGER.error("Error when execute step of Record with uid {} in file {} ExpressionException", code, ex.getFilename(), ex);
            status = Result.INFERNO;
        } catch (final Exception ex) {
            LOGGER.error("Error when execute step of Record with uid {}", code, ex);
            status = Result.ERROR;
        }

        return new MessageProcessResult(status, provider.asBytes());
    }

    /**
     * Sets the service.
     *
     * @param service
     *            the service
     */
    public void setService(final IFormsService service) {
        this.service = service;
    }

}
