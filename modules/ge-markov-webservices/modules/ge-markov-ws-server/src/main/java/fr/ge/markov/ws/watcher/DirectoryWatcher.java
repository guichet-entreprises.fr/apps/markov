/**
 *
 */
package fr.ge.markov.ws.watcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.ws.v1.bean.ResponseQueueMessageBean;
import fr.ge.markov.ws.v1.service.IFormsRestService;
import fr.ge.markov.ws.v1.service.IQueueMessageRestService;

/**
 * The Class DirectoryWatcher.
 *
 * @author bsadil
 */
public class DirectoryWatcher extends AbstractWatcher {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryWatcher.class);

    /** Buffer size. */
    protected static final int BUFFER_SIZE = 8192;

    /** the ZIP extension of record. */
    protected static final String ZIP_EXTENSION = ".zip";

    /** the log extension of record. */
    protected static final String LOG_EXTENSION = ".log";

    /** THE DIGEST_ALGORITHM_SHA1 extension of record. */
    protected static final String SHA1_EXTENSION = ".sha1";

    /** THE DIGEST_ALGORITHM_SHA1 extension of record. */
    protected static final String MD5_EXTENSION = ".md5";

    /** THE DIGEST_ALGORITHM_SHA1 algorithm of record. */
    protected static final String DIGEST_ALGORITHM_SHA1 = "sha1";

    /** the inputDirectory to watch. */
    private String inputDirectory;

    /** the errorDirectory. */
    private String errorDirectory;

    /** The forms service. */
    @Autowired
    private IFormsRestService formsRestService;

    @Autowired
    private IQueueMessageRestService queueMessageRestService;

    private int maxAwaitMessages;

    /**
     * Inits.
     *
     * @throws IOException
     *             an {@link IOException}
     */
    public void init() throws IOException {

        this.sendRecordTomarkov(new File(this.inputDirectory));
    }

    /**
     * Read the file and calculate checksum with a specific algorithm.
     *
     * @param file
     *            the file to read
     * @param algorithm
     *            algorithm
     * @return the SHA-1 using byte
     * @throws TechnicalException
     *             the technical exception
     */
    public byte[] createChecksum(final File file, final String algorithm) throws TechnicalException {
        try (InputStream fis = new FileInputStream(file)) {
            final MessageDigest digest = MessageDigest.getInstance(algorithm);
            int n = 0;
            final byte[] buffer = new byte[BUFFER_SIZE];

            while (n != -1) {
                n = fis.read(buffer);
                if (n > 0) {
                    digest.update(buffer, 0, n);
                }
            }
            return digest.digest();
        } catch (final NoSuchAlgorithmException ex) {
            throw new TechnicalException(String.format("Unable to retrieve %s digest algorithm", algorithm), ex);
        } catch (final FileNotFoundException ex) {
            throw new TechnicalException(String.format("File '%s' not found", file), ex);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to read '%s' file", file), ex);
        }
    }

    void sendRecordTomarkov(File f) {
        final SearchResult<ResponseQueueMessageBean> result = this.queueMessageRestService.search( //
                0L, //
                (this.maxAwaitMessages * 2), //
                Arrays.asList( //
                        new SearchQueryFilter("queueCode:" + MessageQueueStatusEnum.AWAIT.toString()), //
                        new SearchQueryFilter("worker", ":", "")), //
                null);

        if (null != result && result.getTotalResults() <= (this.maxAwaitMessages / 2)) {
            int limit = (this.maxAwaitMessages - new Long(result.getTotalResults()).intValue());
            FileUtils.listFiles(f, new String[] { "sha1" }, true) //
                    .stream() //
                    .sorted(Comparator.comparing(File::lastModified)) //
                    .limit(limit) //
                    .forEach((file) -> {
                        // -->Do not process files that are still awaiting
                        final String basePath = FilenameUtils.removeExtension(file.toPath().toString());
                        if (!result.getContent().stream().anyMatch(r -> r.getUid().equals(basePath))) {
                            LOGGER.debug("Processing file '{}' modified {}", file.getName(), Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime());
                            this.processZipFile(file.toPath());
                        }
                    });
        }
    }

    /**
     * Process all events for keys queued to the watchService.
     *
     * @throws TechnicalException
     *             the technical exception
     */
    @Override
    public void watch() throws TechnicalException {
        this.sendRecordTomarkov(new File(this.inputDirectory));
    }

    /**
     * upload Zip to markov.
     *
     * @param sha1Path
     *            the path of record
     */
    public void processZipFile(final Path sha1Path) {

        final String basePath = FilenameUtils.removeExtension(sha1Path.toString());

        final Path resourcePath = Paths.get(basePath + ZIP_EXTENSION);
        final Path md5Path = Paths.get(basePath + MD5_EXTENSION);

        try {
            final byte[] expectedSha1AsBytes = this.readHashFile(sha1Path);

            // calculate the SHA1 value of Zip File
            final byte[] actualSha1AsBytes = new HexBinaryAdapter().marshal(this.createChecksum(resourcePath.toFile(), DIGEST_ALGORITHM_SHA1)).getBytes();

            // compare SHA1 file already exist in FS and the
            // DIGEST_ALGORITHM_SHA1
            // value of Zip File
            if (Arrays.equals(expectedSha1AsBytes, actualSha1AsBytes)) {
                this.upload(resourcePath);
                md5Path.toFile().delete();
                sha1Path.toFile().delete();
                resourcePath.toFile().delete();
            } else {
                this.moveToErrorDirectory(resourcePath);
                md5Path.toFile().delete();
                sha1Path.toFile().delete();
            }

        } catch (final TechnicalException ex) {
            LOGGER.warn("Upload file '{}' : {}", resourcePath, ex.getMessage(), ex);
        }
    }

    /**
     * Uploads.
     *
     * @param resourcePath
     *            the resource path
     */
    private void upload(final Path resourcePath) {
        String uid = null;
        final Path fileName = resourcePath.getFileName();
        if (fileName != null) {
            uid = FilenameUtils.removeExtension(fileName.toString());
        }

        try (InputStream inputStream = new FileInputStream(resourcePath.toFile())) {
            this.formsRestService.uploadForms(inputStream, uid);
        } catch (final FileNotFoundException ex) {
            throw new TechnicalException(String.format("File '%s' not found", resourcePath), ex);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to read '%s' file", resourcePath), ex);
        }
    }

    /**
     * Moves to error directory.
     *
     * @param resourcePath
     *            the resource path
     */
    private void moveToErrorDirectory(final Path resourcePath) {
        String uid = null;
        final Path fileName = resourcePath.getFileName();
        if (fileName != null) {
            uid = FilenameUtils.removeExtension(fileName.toString());
        }

        try {
            FileUtils.moveFile(resourcePath.toFile(), new File(this.errorDirectory + File.separator + uid + ZIP_EXTENSION));
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to move file '%s' from '%s' to '%s'", resourcePath.getFileName(), resourcePath.getParent(), this.errorDirectory), ex);
        }

        final List<String> lines = Arrays.asList("The zip File is corrupted,the sha1 calculated is different of existing sha1 in the FS");
        final Path file = Paths.get(this.errorDirectory + File.separator + uid + LOG_EXTENSION);

        try {
            Files.write(file, lines, StandardCharsets.UTF_8);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to write log file '%s'", file), ex);
        }
    }

    /**
     * Reads a hash file.
     *
     * @param path
     *            the path
     * @return the byte[]
     * @throws TechnicalException
     *             the technical exception
     */
    private byte[] readHashFile(final Path path) throws TechnicalException {
        try (InputStream stream = new FileInputStream(path.toString())) {
            // get the value of DIGEST_ALGORITHM_SHA1 file already exist in FS
            return IOUtils.toByteArray(stream);
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to read SHA1 from file " + path, ex);
        }
    }

    /**
     * Sets the input directory.
     *
     * @param inputDirectory
     *            the inputDirectory to set
     * @return the directory watcher
     */
    public DirectoryWatcher setInputDirectory(final String inputDirectory) {
        this.inputDirectory = inputDirectory;
        return this;
    }

    /**
     * Sets the error directory.
     *
     * @param errorDirectory
     *            the errorDirectory to set
     * @return the directory watcher
     */
    public DirectoryWatcher setErrorDirectory(final String errorDirectory) {
        this.errorDirectory = errorDirectory;
        return this;
    }

    /**
     * Sets the forms REST service.
     *
     * @param formsService
     *            the formsService to set
     * @return the directory watcher
     */
    public DirectoryWatcher setFormsRestService(final IFormsRestService formsService) {
        this.formsRestService = formsService;
        return this;
    }

    /**
     * Sets the queue message REST service.
     *
     * @param queueMessageRestService
     *            the queueMessageRestService to set
     * @return the directory watcher
     */
    public DirectoryWatcher setQueueMessageRestService(final IQueueMessageRestService queueMessageRestService) {
        this.queueMessageRestService = queueMessageRestService;
        return this;
    }

    /**
     * Mutateur sur l'attribut {@link #maxAwaitMessages}.
     *
     * @param maxAwaitMessages
     *            la nouvelle valeur de l'attribut maxAwaitMessages
     */
    public DirectoryWatcher setMaxAwaitMessages(int maxAwaitMessages) {
        this.maxAwaitMessages = maxAwaitMessages;
        return this;
    }
}
