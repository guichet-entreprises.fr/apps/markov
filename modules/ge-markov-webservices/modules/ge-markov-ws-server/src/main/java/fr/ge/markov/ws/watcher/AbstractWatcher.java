/**
 * 
 */
package fr.ge.markov.ws.watcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.markov.ws.watcher.togglz.MarkovFeatures;

/**
 * Abstract Watcher class.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public abstract class AbstractWatcher implements Runnable {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractWatcher.class);

    private MarkovFeatures feature;

    @Autowired
    public abstract void watch();

    public void run() {
        for (;;) {
            if (null != this.feature && this.feature.isActive()) {
                this.watch();
            }
        }
    }

    /**
     * Accesseur sur l'attribut {@link #feature}.
     *
     * @return String feature
     */
    public MarkovFeatures getFeature() {
        return feature;
    }

    /**
     * Mutateur sur l'attribut {@link #feature}.
     *
     * @param feature
     *            la nouvelle valeur de l'attribut feature
     */
    public AbstractWatcher setFeature(final MarkovFeatures feature) {
        this.feature = feature;
        return this;
    }
}
