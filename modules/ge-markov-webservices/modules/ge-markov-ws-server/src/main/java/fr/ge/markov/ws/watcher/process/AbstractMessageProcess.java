/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.watcher.process;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.markov.ws.watcher.process.MessageProcessResult.Result;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public abstract class AbstractMessageProcess implements IMessageProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMessageProcess.class);

    @Autowired
    private IStorageRestService service;

    @Autowired
    private Engine engine;

    protected abstract String getQueueCode();

    public MessageProcessResult store(final String code, final byte[] resourceAsBytes, final Consumer<byte[]> writer) {
        try (InputStream in = new ByteArrayInputStream(resourceAsBytes)) {
            final SpecificationLoader loader = this.engine.loader(new ZipProvider(resourceAsBytes));
            final List<SearchQueryFilter> metas = new ArrayList<>();
            metas.addAll( //
                    Arrays.asList(//
                            new SearchQueryFilter("author", loader.description().getAuthor()), //
                            new SearchQueryFilter("description", loader.description().getDescription()), //
                            new SearchQueryFilter("title", loader.description().getTitle()) //
                    ) //
            );

            Optional.of(loader) //
                    .filter(l -> null != l) //
                    .map(SpecificationLoader::meta) //
                    .filter(m -> null != m) //
                    .map(FormSpecificationMeta::getMetas) //
                    .filter(l -> null != l) //
                    .orElse(new ArrayList<MetaElement>()) //
                    .forEach(m -> metas.add(new SearchQueryFilter(StringUtils.join(m.getName(), ":", m.getValue()))));

            final Response response = this.getService().store( //
                    this.getQueueCode(), //
                    new Attachment("storageUploadFile", MediaType.APPLICATION_OCTET_STREAM, resourceAsBytes), //
                    String.format("%s.zip", code), //
                    code, //
                    metas //
            );
            if (Status.OK.getStatusCode() == response.getStatus()) {
                LOGGER.info("Archive record {} to the storage queue {} with success", code, this.getQueueCode());
                return new MessageProcessResult(Result.SUCCESS, null);
            } else {
                LOGGER.warn("Unable to archive record {} : storage returned {}", code, response.getStatus());
            }
        } catch (final InternalServerErrorException | IOException | ProcessingException ex) {
            final Throwable connectError = Optional.ofNullable(ex.getCause()) //
                    .filter(InternalServerErrorException.class::isInstance) //
                    .map(Throwable::getCause) //
                    .filter(ConnectException.class::isInstance) //
                    .orElse(null);

            if (null == connectError) {
                LOGGER.warn("Error occured while archiving record {} : {}", code, ex.getMessage());
            } else {
                LOGGER.error("Error occured while archiving record {} : unable to connect to storage", code);
            }
        }

        return new MessageProcessResult(Result.RETRY, null);
    }

    /**
     * Accesseur sur l'attribut {@link #service}.
     *
     * @return IStorageRestService service
     */
    public IStorageRestService getService() {
        return this.service;
    }

    /**
     * Mutateur sur l'attribut {@link #service}.
     *
     * @param service
     *     la nouvelle valeur de l'attribut service
     */
    public void setService(final IStorageRestService service) {
        this.service = service;
    }

    /**
     * Accesseur sur l'attribut {@link #engine}.
     *
     * @return Engine engine
     */
    public Engine getEngine() {
        return this.engine;
    }

    /**
     * Mutateur sur l'attribut {@link #engine}.
     *
     * @param engine
     *     la nouvelle valeur de l'attribut engine
     */
    public void setEngine(final Engine engine) {
        this.engine = engine;
    }

}
