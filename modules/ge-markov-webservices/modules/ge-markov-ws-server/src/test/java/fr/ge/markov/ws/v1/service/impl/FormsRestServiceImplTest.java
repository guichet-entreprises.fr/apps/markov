/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.v1.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.togglz.junit.TogglzRule;

import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.ws.v1.service.IFormsRestService;
import fr.ge.markov.ws.watcher.togglz.MarkovFeatures;

/**
 * Tests {@link IssueServiceImpl}.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/service-context.xml", "classpath:spring/ws-server-cxf-context.xml" })
public class FormsRestServiceImplTest {

    @Rule
    public TogglzRule togglzRule = TogglzRule.allDisabled(MarkovFeatures.class);

    @Autowired
    private IFormsRestService formsRestService;

    @Autowired
    private IQueueMessageService queueMessageDataService;

    @Before
    public void setUp() throws Exception {
        reset(this.queueMessageDataService);
    }

    /**
     * Tests {@link IssueServiceImpl#get(long)}.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUploadForms() throws Exception {

        final InputStream attachmentZip = this.getClass().getResourceAsStream("/zip/2017-02-16_2016-11-VHB-LKD-55.zip");

        final Response response = this.formsRestService.uploadForms(attachmentZip, "2016-11-VHB-LKD-55");

        verify(this.queueMessageDataService).add(eq(MessageQueueStatusEnum.AWAIT.toString()), any(byte[].class), eq("2016-11-VHB-LKD-55"));

        assertEquals(Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void testUploadNoUid() throws Exception {

        final InputStream attachmentZip = this.getClass().getResourceAsStream("/zip/2017-02-16_2016-11-VHB-LKD-55.zip");

        final Response response = this.formsRestService.uploadForms(attachmentZip, null);

        verify(this.queueMessageDataService, times(0)).add(any(), any(byte[].class), any());

        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
    }

}
