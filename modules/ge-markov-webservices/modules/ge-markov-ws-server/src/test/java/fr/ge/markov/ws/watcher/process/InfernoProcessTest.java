/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.watcher.process;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.markov.ws.watcher.process.MessageProcessResult.Result;

/**
 * @author Christian Cougourdan
 *
 */
public class InfernoProcessTest extends AbstractTest {

    @Mock
    private IStorageRestService service;

    @Mock
    private Engine engine;

    @InjectMocks
    private InfernoMessageProcess process;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.process.setService(this.service);
    }

    @Test
    public void testOk() throws Exception {
        final String code = "2017-03-REC-ORD-01";
        final byte[] resourceAsBytes = this.resourceAsBytes("storage.zip");

        final List<SearchQueryFilter> metas = Arrays.asList(//
                new SearchQueryFilter("author", "2016-12-USR-AAA-41"), //
                new SearchQueryFilter("description", "Form description"), //
                new SearchQueryFilter("title", "Test de Markov ERROR") //
        );

        when(this.engine.getEngineProperties()).thenReturn(new Configuration(null));
        when(this.engine.loader(any())).thenReturn(SpecificationLoader.create(new ZipProvider(resourceAsBytes)));
        when(this.service.store(eq("inferno"), any(), eq(String.format("%s.zip", code)), eq(code), eq(metas))).thenReturn(Response.ok().build());

        final MessageProcessResult actual = this.process.accept(code, resourceAsBytes, null);

        verify(this.service).store(eq("inferno"), any(), eq(String.format("%s.zip", code)), eq(code), eq(metas));

        assertThat(actual, //
                allOf( //
                        hasProperty("status", equalTo(Result.SUCCESS)), //
                        hasProperty("resource", nullValue()) //
                ) //
        );
    }
}
