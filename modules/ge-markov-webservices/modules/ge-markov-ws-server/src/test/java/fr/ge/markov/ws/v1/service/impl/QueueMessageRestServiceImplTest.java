/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.v1.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.togglz.junit.TogglzRule;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.service.IQueueMessageFileService;
import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.ws.v1.bean.ResponseQueueMessageBean;
import fr.ge.markov.ws.v1.service.IQueueMessageRestService;
import fr.ge.markov.ws.watcher.togglz.MarkovFeatures;

/**
 * @author Christian Cougourdan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/service-context.xml", "classpath:spring/ws-server-cxf-context.xml" })
public class QueueMessageRestServiceImplTest {

    @Rule
    public TogglzRule togglzRule = TogglzRule.allDisabled(MarkovFeatures.class);

    @Autowired
    private IQueueMessageRestService restService;

    @Autowired
    private IQueueMessageService queueMessageDataService;

    @Autowired
    private IQueueMessageFileService queueMessageFileDataService;

    @Before
    public void setUp() throws Exception {
        reset(this.queueMessageDataService, this.queueMessageFileDataService);
    }

    @Test
    public void testDownload() throws Exception {
        final String uid = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] recordContent = "Test Record Zip Content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(uid).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(recordContent).setUpdated(now);

        when(this.queueMessageDataService.findByUid(uid)).thenReturn(queueMessage);
        when(this.queueMessageFileDataService.findByIdMessage(queueMessage.getId())).thenReturn(queueMessageFile);

        final Response response = this.restService.download(uid);
        final byte[] actual = (byte[]) response.getEntity();

        verify(this.queueMessageDataService).findByUid(uid);
        verify(this.queueMessageFileDataService).findByIdMessage(queueMessage.getId());

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE), equalTo("application/zip"));
        assertThat(response.getHeaderString(HttpHeaders.CONTENT_DISPOSITION), equalTo(String.format("attachment; filename=%s.zip", uid)));
        assertThat(response.getLastModified(), equalTo(now));
        assertThat(actual, equalTo(recordContent));
    }

    @Test
    public void testDownloadUnknown() throws Exception {
        final String uid = "2017-03-REC-ORD-01";

        when(this.queueMessageDataService.findByUid(uid)).thenReturn(null);

        final Response response = this.restService.download(uid);

        verify(this.queueMessageDataService).findByUid(uid);
        verify(this.queueMessageFileDataService, times(0)).findByIdMessage(any(long.class));

        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));
    }

    @Test
    public void testDownloadByMessageId() {
        final String uid = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] recordContent = "Test Record Zip Content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(uid).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(recordContent).setUpdated(now);

        when(this.queueMessageDataService.findById(queueMessage.getId())).thenReturn(queueMessage);
        when(this.queueMessageFileDataService.findByIdMessage(queueMessage.getId())).thenReturn(queueMessageFile);

        final Response response = this.restService.download(queueMessage.getId());
        final byte[] actual = (byte[]) response.getEntity();

        verify(this.queueMessageDataService).findById(queueMessage.getId());
        verify(this.queueMessageFileDataService).findByIdMessage(queueMessage.getId());

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE), equalTo("application/zip"));
        assertThat(response.getHeaderString(HttpHeaders.CONTENT_DISPOSITION), equalTo(String.format("attachment; filename=%s.zip", uid)));
        assertThat(response.getLastModified(), equalTo(now));
        assertThat(actual, equalTo(recordContent));
    }

    @Test
    public void testDownloadByMessageIdUnknown() throws Exception {
        final long messageId = 42L;
        final Response response = this.restService.download(messageId);

        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));

        verify(this.queueMessageDataService).findById(messageId);
    }

    @Test
    public void testMove() {

        when(this.queueMessageDataService.move(eq(51L), eq(MessageQueueStatusEnum.SUCCESS.toString()), any())).thenReturn(51L);

        final Response response = this.restService.move(51L, MessageQueueStatusEnum.SUCCESS.toString());

        assertThat(response, //
                hasProperty("status", equalTo(200)) //
        );
    }

    @Test
    public void testDelete() {

        when(this.queueMessageDataService.deleteById(51L)).thenReturn(51L);

        final Response response = this.restService.delete(51L);

        assertThat(response, //
                hasProperty("status", equalTo(200)) //
        );
    }

    @Test
    public void testUpload() throws IOException {

        final InputStream attachmentZip = this.getClass().getResourceAsStream("/zip/2017-03-RDJ-JGX-43.zip");

        final QueueMessageBean queueMessage = new QueueMessageBean().setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid("2016-11-VHB-LKD-55").setUpdated(new Date());
        when(this.queueMessageDataService.deleteByUid("2017-03-RDJ-JGX-43")).thenReturn(1L);

        final byte[] zip = IOUtils.toByteArray(attachmentZip);

        when(this.queueMessageDataService.add("AWAIT", zip, "2017-03-RDJ-JGX-43")).thenReturn(queueMessage);

        final Response response = this.restService.upload("AWAIT", true, new Attachment("file", "application/octet-stream", zip));

        assertThat(response, //
                hasProperty("status", equalTo(200)) //
        );

        verify(this.queueMessageDataService).deleteByUid("2017-03-RDJ-JGX-43");
    }

    @Test
    public void testUploadOverwritePreviousOne() throws IOException {

        final InputStream attachmentZip = this.getClass().getResourceAsStream("/zip/2017-03-RDJ-JGX-43.zip");

        final QueueMessageBean queueMessage = new QueueMessageBean().setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid("2016-11-VHB-LKD-55").setUpdated(new Date());
        when(this.queueMessageDataService.deleteByUid("2017-03-RDJ-JGX-43")).thenReturn(1L);

        final byte[] zip = IOUtils.toByteArray(attachmentZip);

        when(this.queueMessageDataService.add("AWAIT", zip, "2017-03-RDJ-JGX-43")).thenReturn(queueMessage);

        final Response response = this.restService.upload("AWAIT", false, new Attachment("file", "application/octet-stream", zip));

        assertThat(response, //
                hasProperty("status", equalTo(200)) //
        );

        verify(this.queueMessageDataService, never()).deleteByUid("2017-03-RDJ-JGX-43");
    }

    @Test
    public void testUploadException() throws IOException {

        final QueueMessageBean queueMessage = new QueueMessageBean().setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid("2016-11-VHB-LKD-55").setUpdated(new Date());
        when(this.queueMessageDataService.deleteByUid("2017-03-RDJ-JGX-43")).thenReturn(1L);

        final byte[] zip = null;

        when(this.queueMessageDataService.add("AWAIT", zip, "2017-03-RDJ-JGX-43")).thenReturn(queueMessage);

        final Response response = this.restService.upload("AWAIT", true, new Attachment("file", "application/octet-stream", zip));

        assertThat(response, //
                hasProperty("status", equalTo(500)) //
        );

    }

    @Test
    public void testSearch() {
        final String uid = "2017-03-REC-ORD-02";
        final Date now = new Date();

        final ResponseQueueMessageBean queueMessage = new ResponseQueueMessageBean().setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(uid).setUpdated(now);

        final SearchResult<ResponseQueueMessageBean> dataSearchResult = new SearchResult<>(Long.parseLong(SearchQuery.DEFAULT_START_INDEX), Long.parseLong(SearchQuery.DEFAULT_MAX_RESULTS));
        dataSearchResult.setTotalResults(3L);
        dataSearchResult.setContent(Arrays.asList(queueMessage, queueMessage, queueMessage));

        when(this.queueMessageDataService.search(any(), eq(ResponseQueueMessageBean.class))).thenReturn(dataSearchResult);

        final SearchResult<ResponseQueueMessageBean> response = this.restService.search(Long.parseLong(SearchQuery.DEFAULT_START_INDEX), Long.parseLong(SearchQuery.DEFAULT_MAX_RESULTS),
                Arrays.asList(new SearchQueryFilter("queueCode:AWAIT")), null);

        assertThat(response, //
                hasProperty("totalResults", equalTo(3L)) //
        );
    }

}
