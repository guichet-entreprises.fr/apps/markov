/**
 *
 */
package fr.ge.markov.ws.watcher;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.ws.v1.bean.ResponseQueueMessageBean;
import fr.ge.markov.ws.v1.service.impl.FormsRestServiceImpl;
import fr.ge.markov.ws.v1.service.impl.QueueMessageRestServiceImpl;

/**
 * @author bsadil
 *
 */
public class DirectoryWatcherTest {

    private static String ROOT_DIRECTORY = "target/ge/records";

    private static String ERROR_DIRECTORY = "target/errors";

    private File rootdirectory = null;

    private File errorDirectory = null;

    @Mock
    private FormsRestServiceImpl formsRestService;

    @Mock
    private QueueMessageRestServiceImpl queueMessageRestService;

    private DirectoryWatcher watcher = null;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        this.rootdirectory = new File(ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();

        this.errorDirectory = new File(ERROR_DIRECTORY);
        this.errorDirectory.mkdirs();

        this.watcher = new DirectoryWatcher() //
                .setInputDirectory(ROOT_DIRECTORY) //
                .setErrorDirectory(ERROR_DIRECTORY) //
                .setFormsRestService(this.formsRestService) //
                .setQueueMessageRestService(this.queueMessageRestService);

        this.watcher.init();
    }

    /**
     * the sha1 calculated is different of existing sha1 in the FS
     *
     * @throws Exception
     */
    @Test
    public void uploadZipErrorTest() throws Exception {
        SearchResult<ResponseQueueMessageBean> result = new SearchResult<ResponseQueueMessageBean>();
        result.setTotalResults(1L);
        when(this.queueMessageRestService.search( //
                eq(0L), //
                eq(1L), //
                eq(Arrays.asList( //
                        new SearchQueryFilter("queueCode:" + MessageQueueStatusEnum.AWAIT.toString()))), //
                eq(null)) //
        ).thenReturn(result);

        final File file2 = new File(ROOT_DIRECTORY + File.separator + "test.zip");
        file2.createNewFile();
        final File file1 = new File(ROOT_DIRECTORY + File.separator + "test.sha1");
        file1.createNewFile();
        final File file3 = new File(ROOT_DIRECTORY + File.separator + "test.md5");
        file3.createNewFile();

        this.watcher.processZipFile(Paths.get(file1.getPath()));
        assertEquals(file2.exists(), false);
        assertEquals(file1.exists(), false);
        assertEquals(file3.exists(), false);
        assertEquals(new File(ERROR_DIRECTORY + File.separator + "test.zip").exists(), true);
        assertEquals(new File(ERROR_DIRECTORY + File.separator + "test.log").exists(), true);

        file1.delete();
        file2.delete();
    }

    /**
     * the sha1 calculated equals to existing sha1 in the FS
     *
     * @throws Exception
     */
    @Test
    public void uploadZipTest() throws Exception {

        final File file = new File(ROOT_DIRECTORY + File.separator + "test.zip");
        file.createNewFile();
        SearchResult<ResponseQueueMessageBean> result = new SearchResult<ResponseQueueMessageBean>();
        result.setTotalResults(1L);
        when(this.queueMessageRestService.search( //
                eq(0L), //
                eq(1L), //
                eq(Arrays.asList( //
                        new SearchQueryFilter("queueCode:" + MessageQueueStatusEnum.AWAIT.toString()))), //
                eq(null)) //
        ).thenReturn(result);

        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.sha1"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());
        FileUtils.writeByteArrayToFile(new File(ROOT_DIRECTORY + File.separator + "test.md5"), new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());
        this.watcher.processZipFile(Paths.get(ROOT_DIRECTORY + File.separator + "test.sha1"));
        assertEquals(file.exists(), false);
        verify(this.formsRestService).uploadForms(any(), any());
        file.delete();

    }

    @After
    public void cleanUp() throws IOException {
        FileUtils.deleteDirectory(this.errorDirectory);
        FileUtils.deleteDirectory(this.rootdirectory);
    }

}
