/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.watcher.process;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.markov.service.impl.FormsServiceImpl;
import fr.ge.markov.ws.watcher.process.MessageProcessResult.Result;

/**
 * @author Christian Cougourdan
 *
 */
public class StepExecuteMessageProcessTest extends AbstractTest {

    /** The forms service. */
    @Mock
    private FormsServiceImpl service;

    @InjectMocks
    private StepExecuteMessageProcess messageProcess;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.messageProcess.setService(this.service);
    }

    @Test
    public void testSuccess() throws InterruptedException {
        final String code = "2017-03-REC-ORD-01";
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);

        when(this.service.handleStep(any())).thenReturn(false);

        final MessageProcessResult actual = this.messageProcess.accept(code, resourceAsBytes, null);

        assertThat(actual, //
                allOf( //
                        hasProperty("status", equalTo(Result.SUCCESS)), //
                        hasProperty("resource", equalTo(resourceAsBytes)) //
                ) //
        );
    }

    @Test
    public void testContinue() throws InterruptedException {
        final String code = "2017-03-REC-ORD-01";
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);

        final Consumer<byte[]> writer = mock(Consumer.class);

        when(this.service.handleStep(any())).thenReturn(true, false);

        final MessageProcessResult actual = this.messageProcess.accept(code, resourceAsBytes, writer);

        assertThat(actual, //
                allOf( //
                        hasProperty("status", equalTo(Result.SUCCESS)), //
                        hasProperty("resource", equalTo(resourceAsBytes)) //
                ) //
        );

        verify(writer).accept(any());
    }

    @Test
    public void testExpressionError() throws InterruptedException {
        final String code = "2017-03-REC-ORD-01";
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);

        when(this.service.handleStep(any())).thenThrow(new ExpressionException("Script error", "script.js", "a == b", 1, 1));

        final MessageProcessResult actual = this.messageProcess.accept(code, resourceAsBytes, null);

        assertThat(actual, //
                allOf( //
                        hasProperty("status", equalTo(Result.INFERNO)), //
                        hasProperty("resource", equalTo(resourceAsBytes)) //
                ) //
        );
    }

    @Test
    public void testError() throws InterruptedException {
        final String code = "2017-03-REC-ORD-01";
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);

        when(this.service.handleStep(any())).thenThrow(new TechnicalException("Unknown error"));

        final MessageProcessResult actual = this.messageProcess.accept(code, resourceAsBytes, null);

        assertThat(actual, //
                allOf( //
                        hasProperty("status", equalTo(Result.ERROR)), //
                        hasProperty("resource", equalTo(resourceAsBytes)) //
                ) //
        );
    }
}
