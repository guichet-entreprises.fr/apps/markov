/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.markov.ws.support.filter;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.junit.Test;
import org.slf4j.MDC;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

/**
 *
 * @author Christian Cougourdan
 *
 */
public class LogContextWsFilterTest {

    @Test
    public void testEmpty() throws Exception {
        final Map<String, Object> actual = new HashMap<>();

        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final FilterChain chain = (req, res) -> Arrays.asList("correlationId", "userId").forEach(key -> actual.put(key, MDC.get(key)));

        MDC.clear();
        new LogContextWsFilter().doFilter(request, response, chain);

        assertThat(actual, //
                allOf( //
                        hasEntry(equalTo("correlationId"), notNullValue()), //
                        hasEntry(equalTo("userId"), nullValue()) //
                ) //
        );
    }

    @Test
    public void testNotHttp() throws Exception {
        final Map<String, Object> actual = new HashMap<>();

        final ServletRequest request = mock(ServletRequest.class);
        final ServletResponse response = mock(ServletResponse.class);
        final FilterChain chain = (req, res) -> Arrays.asList("correlationId", "userId").forEach(key -> actual.put(key, MDC.get(key)));

        MDC.clear();
        new LogContextWsFilter().doFilter(request, response, chain);

        assertThat(actual, //
                allOf( //
                        hasEntry(equalTo("correlationId"), notNullValue()), //
                        hasEntry(equalTo("userId"), nullValue()) //
                ) //
        );
    }

    @Test
    public void testFilled() throws Exception {
        final Map<String, Object> actual = new HashMap<>();

        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final FilterChain chain = (req, res) -> Arrays.asList("correlationId", "userId").forEach(key -> actual.put(key, MDC.get(key)));

        final String correlationId = UUID.randomUUID().toString();
        request.addHeader("X-Correlation-ID", correlationId);
        request.addHeader("X-User-ID", "2019-02-JON-DOE-42");

        MDC.clear();
        new LogContextWsFilter().doFilter(request, response, chain);

        assertThat(actual, //
                allOf( //
                        hasEntry(equalTo("correlationId"), equalTo(correlationId)), //
                        hasEntry(equalTo("userId"), equalTo("2019-02-JON-DOE-42")) //
                ) //
        );
    }

}
