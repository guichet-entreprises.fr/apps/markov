/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.v1.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.togglz.core.util.FeatureStateStorageWrapper;
import org.togglz.junit.TogglzRule;

import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageSummaryBean;
import fr.ge.markov.ws.v1.service.IMonitoringRestService;
import fr.ge.markov.ws.watcher.togglz.MarkovFeatures;

/**
 * @author Christian Cougourdan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/service-context.xml", "classpath:spring/ws-server-cxf-context.xml" })
public class MonitoringRestServiceImplTest {

    @Autowired
    private IMonitoringRestService restService;

    @Autowired
    private IQueueMessageService queueMessageDataService;

    @Rule
    public TogglzRule togglzRule = TogglzRule.allDisabled(MarkovFeatures.class);

    private static String ROOT_DIRECTORY = "target/ge/records";

    private File rootdirectory = null;

    @Before
    public void setUp() throws Exception {
        reset(this.queueMessageDataService);

        this.rootdirectory = new File(ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();
    }

    @After
    public void cleanUp() throws IOException {
        FileUtils.deleteDirectory(this.rootdirectory);
    }

    @Test
    public void testSummary() throws Exception {
        when(this.queueMessageDataService.summary()).thenReturn(new QueueMessageSummaryBean().add(MessageQueueStatusEnum.AWAIT.toString(), 12L));

        final Response response = this.restService.summary();
        final QueueMessageSummaryBean actual = (QueueMessageSummaryBean) response.getEntity();

        verify(this.queueMessageDataService).summary();

        assertThat(response, hasProperty("status", equalTo(Status.OK.getStatusCode())));
        assertThat(actual, hasProperty("totalEntries", hasEntry(MessageQueueStatusEnum.AWAIT.toString(), 12L)));
    }

    @Test
    public void testSummaryNoResult() throws Exception {
        when(this.queueMessageDataService.summary()).thenReturn(null);

        final Response response = this.restService.summary();
        final QueueMessageSummaryBean actual = (QueueMessageSummaryBean) response.getEntity();

        verify(this.queueMessageDataService).summary();

        assertThat(response, hasProperty("status", equalTo(Status.NO_CONTENT.getStatusCode())));
        assertThat(actual, nullValue());
    }

    @Test
    public void testGetFeature() throws Exception {
        final FeatureStateStorageWrapper actual = this.restService.getFeature(MarkovFeatures.AWAIT.name());
        assertThat(actual, notNullValue());
        assertThat(actual, hasProperty("enabled", equalTo(false)));
    }

    @Test
    public void testUpdateFeature() throws Exception {
        final Response response = this.restService.updateFeature(MarkovFeatures.AWAIT.name(), false);
        assertThat(response, hasProperty("status", equalTo(Status.OK.getStatusCode())));

        final FeatureStateStorageWrapper actual = this.restService.getFeature(MarkovFeatures.AWAIT.name());
        assertThat(actual, notNullValue());
        assertThat(actual, hasProperty("enabled", equalTo(false)));
    }

    @Test
    public void testCountPending() throws Exception {
        final File file1 = new File(ROOT_DIRECTORY + File.separator + "test.sha1");
        file1.createNewFile();

        final Integer actual = this.restService.pending();
        assertThat(actual, notNullValue());
        assertThat(actual, equalTo(1));
    }
}
