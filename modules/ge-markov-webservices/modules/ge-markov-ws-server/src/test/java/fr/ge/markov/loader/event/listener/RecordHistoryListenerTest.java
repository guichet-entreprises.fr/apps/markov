package fr.ge.markov.loader.event.listener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.MessageEvent;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationHistory;

/**
 * Record history listener test.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordHistoryListenerTest {

    /** The loader. */
    @Mock
    private SpecificationLoader loader;

    /** The event. */
    @Mock
    private MessageEvent event;

    /** The listener. */
    @InjectMocks
    private RecordHistoryListener listener;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnEvent() {
        // mock
        when(this.event.getLoader()).thenReturn(this.loader);
        when(this.event.getMessage()).thenReturn("My message");
        when(this.loader.historyPath()).thenReturn("history.xml");
        when(this.loader.history()).thenReturn(new FormSpecificationHistory());

        // call
        this.listener.onMessageEvent(this.event);

        // check
        final String datePattern = "\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}";
        final String message = "Le dossier a été traité automatiquement. My message";
        final ArgumentCaptor<FormSpecificationHistory> history = ArgumentCaptor.forClass(FormSpecificationHistory.class);
        verify(this.loader).save(eq("history.xml"), history.capture());
        final FormSpecificationHistory actual = history.getValue();

        assertNotNull(actual);
        assertEquals(actual.getEvents().size(), 1);
        assertTrue(Pattern.compile(datePattern).matcher(actual.getEvents().get(0).getdate()).matches());
        assertEquals(actual.getEvents().get(0).getMessage(), message);
    }
}
