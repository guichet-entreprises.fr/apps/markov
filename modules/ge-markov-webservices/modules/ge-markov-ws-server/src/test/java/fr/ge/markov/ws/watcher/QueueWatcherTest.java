/**
 *
 */
package fr.ge.markov.ws.watcher;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
//import org.togglz.junit.TogglzRule;
import org.togglz.junit.TogglzRule;

import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.service.impl.QueueMessageServiceImpl;
import fr.ge.markov.ws.watcher.process.IMessageProcess;
import fr.ge.markov.ws.watcher.process.MessageProcessResult;
import fr.ge.markov.ws.watcher.process.MessageProcessResult.Result;
import fr.ge.markov.ws.watcher.togglz.MarkovFeatures;

/**
 * @author bsadil
 *
 */
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class QueueWatcherTest {

    @Rule
    public TogglzRule togglzRule = TogglzRule.allDisabled(MarkovFeatures.class);

    /**
     * the worker instance of markov
     */
    final private String worker = "worker1";

    @Mock
    private QueueMessageServiceImpl messageService;

    @Mock
    private IMessageProcess messageProcess;

    @InjectMocks
    private QueueWatcher queueWatcher;

    @Mock
    private IRecordStorage recordStorage;

    private static String STORAGE_DIRECTORY = "target/ge/support";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.queueWatcher.setThreadSleep(0) //
                .setWorker(this.worker) //
                .setMessageService(this.messageService) //
                .setMessageProcess(this.messageProcess) //
                .setRecordStorage(this.recordStorage) //
                .setWatchedStatus(MessageQueueStatusEnum.AWAIT) //
                .setFeature(MarkovFeatures.AWAIT) //
        ;

        reset(this.messageService, this.messageProcess, this.recordStorage);
        this.togglzRule.enable(MarkovFeatures.AWAIT);
        this.togglzRule.enable(MarkovFeatures.SUCCESS);
        assertThat(MarkovFeatures.SUCCESS.isActive(), equalTo(true));
        assertThat(MarkovFeatures.AWAIT.isActive(), equalTo(true));
    }

    @Test
    public void testSimple() throws InterruptedException {
        final String uid = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);
        final byte[] updatedResourceAsBytes = "Updated resource content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setWorker(this.worker).setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(uid).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(resourceAsBytes).setUpdated(now);

        when(this.messageService.next(this.worker, MessageQueueStatusEnum.AWAIT)).thenReturn(queueMessage, null, null);
        when(this.messageService.findMessageContent(queueMessage.getId())).thenReturn(queueMessageFile);
        when(this.messageProcess.accept(eq(uid), eq(resourceAsBytes), any())).thenReturn(new MessageProcessResult(Result.SUCCESS, updatedResourceAsBytes));

        final ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(this.queueWatcher);
        executor.awaitTermination(1000, TimeUnit.MILLISECONDS);

        final ArgumentCaptor<QueueMessageBean> messageCaptor = ArgumentCaptor.forClass(QueueMessageBean.class);
        final ArgumentCaptor<QueueMessageFileBean> resourceCaptor = ArgumentCaptor.forClass(QueueMessageFileBean.class);

        verify(this.messageService, atLeastOnce()).next(this.worker, MessageQueueStatusEnum.AWAIT);
        verify(this.messageService, atLeastOnce()).findMessageContent(queueMessage.getId());
        verify(this.messageProcess, atLeastOnce()).accept(eq(uid), eq(resourceAsBytes), any());
        verify(this.messageService, atLeastOnce()).release(messageCaptor.capture(), resourceCaptor.capture());

        assertThat(messageCaptor.getValue().getQueueCode(), equalTo(MessageQueueStatusEnum.SUCCESS.toString()));
        assertThat(resourceCaptor.getValue().getContent(), equalTo(updatedResourceAsBytes));
    }

    @Test
    public void testOtherQueueStatusThanDefault() throws InterruptedException {
        final String uid = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);
        final byte[] updatedResourceAsBytes = "Updated resource content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setWorker(this.worker).setId(51L).setQueueCode(MessageQueueStatusEnum.SUCCESS.toString()).setUid(uid).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(resourceAsBytes).setUpdated(now);

        when(this.messageService.next(this.worker, MessageQueueStatusEnum.SUCCESS)).thenReturn(queueMessage, null, null);
        when(this.messageService.findMessageContent(queueMessage.getId())).thenReturn(queueMessageFile);
        when(this.messageProcess.accept(eq(uid), eq(resourceAsBytes), any())).thenReturn(new MessageProcessResult(Result.RETRY, updatedResourceAsBytes));

        this.queueWatcher.setWatchedStatus(MessageQueueStatusEnum.SUCCESS);

        final ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(this.queueWatcher);
        executor.awaitTermination(1000, TimeUnit.MILLISECONDS);

        final ArgumentCaptor<QueueMessageBean> messageCaptor = ArgumentCaptor.forClass(QueueMessageBean.class);
        final ArgumentCaptor<QueueMessageFileBean> resourceCaptor = ArgumentCaptor.forClass(QueueMessageFileBean.class);

        verify(this.messageService, atLeastOnce()).next(this.worker, MessageQueueStatusEnum.SUCCESS);
        verify(this.messageService, atLeastOnce()).findMessageContent(queueMessage.getId());
        verify(this.messageProcess, atLeastOnce()).accept(eq(uid), eq(resourceAsBytes), any());
        verify(this.messageService, atLeastOnce()).release(messageCaptor.capture(), resourceCaptor.capture());

        assertThat(messageCaptor.getValue().getQueueCode(), equalTo(MessageQueueStatusEnum.SUCCESS.toString()));
        assertThat(resourceCaptor.getValue().getContent(), equalTo(updatedResourceAsBytes));
    }

    @Test
    public void testNullReturnedResource() throws InterruptedException {
        final String uid = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setWorker(this.worker).setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(uid).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(resourceAsBytes).setUpdated(now);

        when(this.messageService.next(this.worker, MessageQueueStatusEnum.AWAIT)).thenReturn(queueMessage, null, null);
        when(this.messageService.findMessageContent(queueMessage.getId())).thenReturn(queueMessageFile);
        when(this.messageProcess.accept(eq(uid), eq(resourceAsBytes), any())).thenReturn(new MessageProcessResult(Result.SUCCESS, null));

        final ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(this.queueWatcher);
        executor.awaitTermination(1000, TimeUnit.MILLISECONDS);

        verify(this.messageService, atLeastOnce()).next(this.worker, MessageQueueStatusEnum.AWAIT);
        verify(this.messageService, atLeastOnce()).findMessageContent(queueMessage.getId());
        verify(this.messageProcess, atLeastOnce()).accept(eq(uid), eq(resourceAsBytes), any());
    }

    @Test
    public void testError() throws InterruptedException {
        final String code = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setWorker(this.worker).setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(code).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(resourceAsBytes).setUpdated(now);

        when(this.messageService.next(this.worker, MessageQueueStatusEnum.AWAIT)).thenReturn(queueMessage, null, null);
        when(this.messageService.findMessageContent(queueMessage.getId())).thenReturn(queueMessageFile);
        when(this.messageProcess.accept(eq(code), eq(resourceAsBytes), any())).thenThrow(new TechnicalException("oups"));

        final ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(this.queueWatcher);
        executor.awaitTermination(1000, TimeUnit.MILLISECONDS);

        final ArgumentCaptor<QueueMessageBean> messageCaptor = ArgumentCaptor.forClass(QueueMessageBean.class);
        final ArgumentCaptor<QueueMessageFileBean> resourceCaptor = ArgumentCaptor.forClass(QueueMessageFileBean.class);

        verify(this.messageService, atLeastOnce()).next(this.worker, MessageQueueStatusEnum.AWAIT);
        verify(this.messageService, atLeastOnce()).findMessageContent(queueMessage.getId());
        verify(this.messageProcess, atLeastOnce()).accept(eq(code), eq(resourceAsBytes), any());
        verify(this.messageService, atLeastOnce()).release(messageCaptor.capture(), resourceCaptor.capture());

        assertThat(messageCaptor.getValue().getQueueCode(), equalTo(MessageQueueStatusEnum.ERROR.toString()));
        assertThat(resourceCaptor.getValue(), nullValue());
    }

    @Test
    public void testErrorRecord() throws InterruptedException, TemporaryException {
        this.togglzRule.enable(MarkovFeatures.AWAIT);
        final String code = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setWorker(this.worker).setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(code).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(resourceAsBytes).setUpdated(now);

        when(this.messageService.next(this.worker, MessageQueueStatusEnum.AWAIT)).thenReturn(queueMessage, null, null);
        when(this.messageService.findMessageContent(queueMessage.getId())).thenReturn(queueMessageFile);
        when(this.messageProcess.accept(eq(code), eq(resourceAsBytes), any())).thenReturn(new MessageProcessResult(Result.ERROR, null));
        doNothing().when(this.recordStorage).storeRecord(code, resourceAsBytes, STORAGE_DIRECTORY);

        final ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(this.queueWatcher);
        executor.awaitTermination(1000, TimeUnit.MILLISECONDS);

        final ArgumentCaptor<QueueMessageBean> messageCaptor = ArgumentCaptor.forClass(QueueMessageBean.class);
        final ArgumentCaptor<QueueMessageFileBean> resourceCaptor = ArgumentCaptor.forClass(QueueMessageFileBean.class);

        verify(this.messageService, atLeastOnce()).next(this.worker, MessageQueueStatusEnum.AWAIT);
        verify(this.messageService, atLeastOnce()).findMessageContent(queueMessage.getId());
        verify(this.messageProcess, atLeastOnce()).accept(eq(code), eq(resourceAsBytes), any());
        verify(this.messageService, atLeastOnce()).release(messageCaptor.capture(), resourceCaptor.capture());

        assertThat(messageCaptor.getValue().getQueueCode(), equalTo(MessageQueueStatusEnum.ERROR.toString()));
        assertThat(resourceCaptor.getValue(), nullValue());
    }

    @Test
    public void testContinue() throws Exception {
        final String uid = "2017-03-REC-ORD-01";
        final Date now = new Date();
        final byte[] resourceAsBytes = "Resource content".getBytes(StandardCharsets.UTF_8);
        final byte[] updatedResourceAsBytes = "Updated resource content".getBytes(StandardCharsets.UTF_8);

        final QueueMessageBean queueMessage = new QueueMessageBean().setWorker(this.worker).setId(51L).setQueueCode(MessageQueueStatusEnum.AWAIT.toString()).setUid(uid).setUpdated(now);
        final QueueMessageFileBean queueMessageFile = new QueueMessageFileBean().setId(42L).setContent(resourceAsBytes).setUpdated(now);

        when(this.messageService.next(this.worker, MessageQueueStatusEnum.AWAIT)).thenReturn(queueMessage, null, null);
        when(this.messageService.findMessageContent(queueMessage.getId())).thenReturn(queueMessageFile);
        when(this.messageProcess.accept(eq(uid), eq(resourceAsBytes), any())).thenReturn(new MessageProcessResult(Result.SUCCESS, updatedResourceAsBytes));

        final ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(this.queueWatcher);
        executor.awaitTermination(1000, TimeUnit.MILLISECONDS);

        final ArgumentCaptor<QueueMessageBean> messageCaptor = ArgumentCaptor.forClass(QueueMessageBean.class);
        final ArgumentCaptor<QueueMessageFileBean> resourceCaptor = ArgumentCaptor.forClass(QueueMessageFileBean.class);

        verify(this.messageService, atLeastOnce()).next(this.worker, MessageQueueStatusEnum.AWAIT);
        verify(this.messageService, atLeastOnce()).findMessageContent(queueMessage.getId());
        verify(this.messageProcess, atLeastOnce()).accept(eq(uid), eq(resourceAsBytes), any());
        verify(this.messageService, atLeastOnce()).release(messageCaptor.capture(), resourceCaptor.capture());

        assertThat(messageCaptor.getValue().getQueueCode(), equalTo(MessageQueueStatusEnum.SUCCESS.toString()));
        assertThat(resourceCaptor.getValue().getContent(), equalTo(updatedResourceAsBytes));
    }

}
