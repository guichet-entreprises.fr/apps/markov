/**
 *
 */
package fr.ge.markov.ws.v1.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author bsadil
 *
 */
@XmlRootElement(name = "messageQueue", namespace = "http://v1.ws.markov.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseQueueMessageBean extends ResponseDatedBean<ResponseQueueMessageBean> {

    /** Status. */
    private String queueCode;

    /**
     * Worker
     */
    private String worker;

    /**
     * uid
     */
    private String uid;

    /**
     * handling
     */
    private boolean handling;

    /**
     * @return the worker
     */
    public String getWorker() {
        return this.worker;
    }

    /**
     * @return the handling
     */
    public boolean isHandling() {
        return this.handling;
    }

    /**
     * @param handling
     *            the handling to set
     */
    public void setHandling(final boolean handling) {
        this.handling = handling;
    }

    /**
     * @param worker
     *            the worker to set
     */
    public ResponseQueueMessageBean setWorker(final String worker) {
        this.worker = worker;
        if (worker != null) {
            this.setHandling(true);
        } else {
            this.setHandling(false);
        }
        return this;
    }

    /**
     * @return the queueCode
     */
    public String getQueueCode() {
        return this.queueCode;
    }

    /**
     * @param queueCode
     *            the queueCode to set
     */
    public ResponseQueueMessageBean setQueueCode(final String queueCode) {
        this.queueCode = queueCode;
        return this;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public ResponseQueueMessageBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

}
