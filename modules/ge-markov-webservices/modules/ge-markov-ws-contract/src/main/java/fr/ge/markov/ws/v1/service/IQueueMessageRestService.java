/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.markov.ws.v1.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.ws.v1.bean.ResponseQueueMessageBean;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Queue message REST service.
 *
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */
@Path("/v1/queue")
public interface IQueueMessageRestService {

    /**
     * Download record file linked to a message.
     *
     * @param uid
     *            record UID
     * @return record file
     */
    @GET
    @Path("{uid}")
    @Produces("application/zip")
    @Operation(summary = "Download a message.", tags = { "Queue Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = "application/zip" //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "204", //
                    description = "Message not found.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response download( //
            @Parameter(description = "Message code", required = true) @PathParam("uid") String uid);

    /**
     * Download record file linked to a message.
     *
     * @param id
     *            queue message ID
     * @return record file
     */
    @GET
    @Path("/message/{id}/content")
    @Produces("application/zip")
    @Operation(summary = "Download a message.", tags = { "Queue Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = "application/zip" //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "204", //
                    description = "Message not found.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response download( //
            @Parameter(description = "Message identifier", required = true) @PathParam("id") long id);

    /**
     * Search for queue messages.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches messages.", tags = { "Queue Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"startIndex\": 0, \"maxResults\": 20, \"totalResults\": 1, \"content\": [ { \"id\": 482, \"created\": 1592903712231, \"updated\": 1592903753326, \"queueCode\": \"AWAIT\", \"worker\": \"markov01\", \"uid\": \"2020-01-REC-ORD-03\", \"handling\": true } ] }") //
                    ) //
            ), //
    })
    SearchResult<ResponseQueueMessageBean> search( //
            @Parameter(description = "Start index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum results") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders") @QueryParam("orders") List<SearchQueryOrder> orders //
    );

    /**
     * Delete a specific queue message.
     *
     * @param id
     *            queue message ID
     * @return
     */
    @DELETE
    @Path("/message/{id:\\d+}")
    @Operation(summary = "Deletes a message.", tags = { "Queue Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "304", //
                    description = "Message not modified.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response delete( //
            @Parameter(description = "Message identifier", required = true) @PathParam("id") long id);

    /**
     * Upload record file in a specific queue.
     *
     * @param queueCode
     *            destination queue
     * @param overwritePreviousOne
     *            true to remove previous corresponding record (same UID)
     * @param attachment
     *            the attachment
     * @return
     */
    @POST
    @Path("{queueCode}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(summary = "Upload record file in a specific queue.", tags = { "Queue Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "500", //
                    description = "Unable to load record file", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "500", //
                    description = "Unable to load record metadata", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response upload( //
            @Parameter(description = "Target queue code", required = true) @PathParam("queueCode") String queueCode, //
            @Parameter(description = "Overwrite previous message", required = true) @QueryParam("overwrite") @DefaultValue("true") boolean overwritePreviousOne, //
            @Parameter(description = "Message content", required = true) Attachment attachment);

    /**
     * Move a message to a specific queue, identified by its code.
     *
     * @param id
     *            queue message ID
     * @param dstQueueCode
     *            destination queue
     * @return
     */
    @PUT
    @Path("/message/{id:\\d+}")
    @Operation(summary = "Moves a message in a specific queue.", tags = { "Queue Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "304", //
                    description = "Message not modified.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response move( //
            @Parameter(description = "Message identifier", required = true) @PathParam("id") long id, //
            @Parameter(description = "Target queue code", required = true) @QueryParam("dst") String dstQueueCode);

}
