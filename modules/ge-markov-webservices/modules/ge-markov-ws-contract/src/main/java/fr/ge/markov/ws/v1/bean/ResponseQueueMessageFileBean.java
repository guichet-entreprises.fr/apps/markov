/**
 * 
 */
package fr.ge.markov.ws.v1.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author bsadil
 *
 */
@XmlRootElement(name = "messageQueue", namespace = "http://v1.ws.markov.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseQueueMessageFileBean extends ResponseDatedBean<ResponseQueueMessageFileBean> {

    /** Status. */
    private String queueCode;

    /**
     * Worker
     */
    private String worker;

    /**
     * uid
     */
    private String uid;

    /**
     * @return the worker
     */
    public String getWorker() {
        return this.worker;
    }

    /**
     * @param worker
     *            the worker to set
     */
    public ResponseQueueMessageFileBean setWorker(final String worker) {
        this.worker = worker;
        return this;
    }

    /**
     * @return the queueCode
     */
    public String getQueueCode() {
        return this.queueCode;
    }

    /**
     * @param queueCode
     *            the queueCode to set
     */
    public ResponseQueueMessageFileBean setQueueCode(final String queueCode) {
        this.queueCode = queueCode;
        return this;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public ResponseQueueMessageFileBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

}
