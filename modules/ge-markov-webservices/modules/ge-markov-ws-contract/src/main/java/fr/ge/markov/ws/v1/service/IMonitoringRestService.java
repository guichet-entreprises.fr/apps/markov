/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.ws.v1.service;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.togglz.core.util.FeatureStateStorageWrapper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Monitoring REST service.
 * 
 * @author Christian Cougourdan
 */
@Path("/v1/monitoring")
public interface IMonitoringRestService {

    /**
     * Gives a summary.
     *
     * @return the summary
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Display monitoring information.", tags = { "Monitoring Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "204", //
                    description = "No summary.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response summary();

    /**
     * Get Togglz feature state.
     * 
     * @param feature
     *            the feature name
     * @return
     */
    @GET
    @Path("/feature/{feature}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get Togglz feature.", tags = { "Monitoring Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("") //
                    ) //
            ), //
    })
    FeatureStateStorageWrapper getFeature( //
            @Parameter(description = "Feature name", required = true) @PathParam("feature") final String feature);

    /**
     * Update Togglz feature state
     * 
     * @param feature
     *            the feature name
     * @param state
     *            the next state
     * @return
     */
    @PUT
    @Path("/feature/{feature}/state/{state}")
    @Operation(summary = "Update Togglz feature.", tags = { "Monitoring Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("") //
                    ) //
            ), //
    })
    Response updateFeature( //
            @Parameter(description = "Feature name", required = true) @PathParam("feature") final String feature, //
            @Parameter(description = "Enable or disable feature state", required = true) @PathParam("state") final boolean state);

    /**
     * Count pending files to be processed into Markov.
     * 
     * @return
     */
    @GET
    @Path("/pending/files")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Display total pending files.", tags = { "Monitoring Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("10") //
                    ) //
            ), //
    })
    Integer pending();
}
