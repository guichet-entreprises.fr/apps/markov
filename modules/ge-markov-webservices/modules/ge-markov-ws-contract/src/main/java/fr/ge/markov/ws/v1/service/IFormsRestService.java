/**
 *
 */
package fr.ge.markov.ws.v1.service;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Forms REST service.
 * 
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */
@Path("/v1/form")
public interface IFormsRestService {

    /**
     * REST method for sending postmail.
     *
     * @param attachmentForms
     *            the attachment forms
     * @param uid
     *            the UID
     * @return The response
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(summary = "Upload a message to AWAIT queue.", tags = { "Forms Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "500", //
                    description = "Message identiifier not found.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "500", //
                    description = "Message cannot be uploaded.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response uploadForms( //
            @Parameter(description = "Input message", required = true) @Multipart(value = "attachmentForms") final InputStream attachmentForms, //
            @Parameter(description = "Message identiifier") @QueryParam("uid") @DefaultValue("") final String uid //
    );

}
