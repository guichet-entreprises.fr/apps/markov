var cerfaFields = {};
cerfaFields['lastname']  = $data.dataGroup.identity.lastname;
cerfaFields['firstname'] = $data.dataGroup.identity.firstname;

var cerfa = pdf.create('models/cerfa14004-02.pdf', cerfaFields);
var cerfaPdf = pdf.save('cerfa14004-02.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Formulaires générés',
        data : [ spec.createData({
            id : 'cerfa14004',
            label : 'Cerfa 14004*2',
            description : 'Cerfa obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
