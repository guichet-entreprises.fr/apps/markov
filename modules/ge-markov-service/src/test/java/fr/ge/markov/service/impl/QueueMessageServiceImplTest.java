/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.service.IQueueMessageFileService;
import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.service.bean.QueueMessageSummaryBean;
import fr.ge.markov.service.data.AbstractDbTest;

/**
 * @author Christian Cougourdan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class QueueMessageServiceImplTest extends AbstractDbTest {

    @Autowired
    private IQueueMessageService service;

    @Autowired
    private IQueueMessageFileService serviceFile;

    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    @Test
    public void testSummary() throws Exception {
        final QueueMessageSummaryBean actual = this.service.summary();

        assertThat(actual.getTotalEntries(), //
                allOf( //
                        hasEntry(MessageQueueStatusEnum.AWAIT.toString(), 3L), //
                        hasEntry(MessageQueueStatusEnum.ERROR.toString(), 1L), //
                        hasEntry(MessageQueueStatusEnum.SUCCESS.toString(), 5L) //
                ) //
        );
    }

    @Test
    public void testAddMessage() throws Exception {
        this.service.add(MessageQueueStatusEnum.AWAIT.toString(), "Nothing".getBytes(Charset.forName("UTF-8")), "worker_1");
    }

    @Test
    public void testfindByStatus() {
        final QueueMessageBean actual = this.service.findByStatus(MessageQueueStatusEnum.AWAIT.toString());

        assertThat(actual, allOf( //
                Arrays.asList( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("updated", notNullValue()), //
                        hasProperty("queueCode", equalTo(MessageQueueStatusEnum.AWAIT.toString())), //
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-50"))) //
        ) //
        );
    }

    @Test
    public void testNext() {
        final QueueMessageBean actual = this.service.next("worker1", MessageQueueStatusEnum.AWAIT);

        assertThat(actual, allOf( //
                Arrays.asList( //
                        hasProperty("id", equalTo(1002L)), //
                        hasProperty("created", notNullValue()), //
                        hasProperty("updated", notNullValue()), //
                        hasProperty("queueCode", equalTo(MessageQueueStatusEnum.AWAIT.toString())), //
                        hasProperty("worker", equalTo("worker1")), //
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-51"))) //
        ) //
        );

    }

    @Test
    public void testfindMessageByContent() {
        final QueueMessageBean queueMessage = this.service.findByStatus(MessageQueueStatusEnum.AWAIT.toString());
        final QueueMessageFileBean actual = this.service.findMessageContent(queueMessage.getId());

        assertThat(actual, allOf( //
                Arrays.asList( //
                        hasProperty("id", equalTo(100L)), //
                        hasProperty("updated", notNullValue()), //
                        hasProperty("created", notNullValue())) //
        ) //
        );
    }

    @Test
    public void testFindByUid() {
        final QueueMessageBean actual = this.service.findByUid("2017-01-ANA-RJL-50");

        assertThat(actual, allOf( //
                Arrays.asList( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("updated", notNullValue()), //
                        hasProperty("queueCode", equalTo(MessageQueueStatusEnum.AWAIT.toString())), //
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-50"))) //
        ) //
        );
    }

    @Test
    public void testRelease() {
        final QueueMessageBean queueMessage = this.service.findByStatus(MessageQueueStatusEnum.AWAIT.toString());
        final QueueMessageFileBean actual = this.service.findMessageContent(queueMessage.getId());

        this.service.release(queueMessage, actual);

        assertThat(queueMessage, allOf( //
                Arrays.asList( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("updated", notNullValue()), //
                        hasProperty("queueCode", equalTo(MessageQueueStatusEnum.AWAIT.toString())), //
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-50"))) //
        ) //
        );
    }

    @Test
    public void testfindAllQueueByStatus() {
        final List<QueueMessageBean> actual = this.service.findAllQueueByStatus(MessageQueueStatusEnum.AWAIT.toString());

        assertEquals(actual.size(), 3);
        assertThat(actual.get(0), allOf(Arrays.asList( //
                hasProperty("id", equalTo(1001L)), //
                hasProperty("updated", notNullValue()), //
                hasProperty("queueCode", equalTo(MessageQueueStatusEnum.AWAIT.toString())), //
                hasProperty("uid", equalTo("2017-01-ANA-RJL-50"))) //
        ));
    }

    @Test
    public void testSearch() {
        final SearchResult<QueueMessageBean> actual = this.service.search(new SearchQuery(0L, 5L), QueueMessageBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(5L)), //
                        hasProperty("totalResults", equalTo(9L)), //
                        hasProperty("content", hasSize(5)) //
                ) //
        );
    }

    @Test
    public void testSearchByQueueCode() {
        final SearchResult<QueueMessageBean> actual = this.service.search(new SearchQuery(0L, 5L).addFilter("queueCode", MessageQueueStatusEnum.AWAIT.toString()), QueueMessageBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(5L)), //
                        hasProperty("totalResults", equalTo(3L)), //
                        hasProperty("content", hasSize(3)) //
                ) //
        );
    }

    @Test
    public void testSearchByWorker() {
        final SearchResult<QueueMessageBean> actual = this.service.search(new SearchQuery(0L, 5L) //
                .addFilter("queueCode", MessageQueueStatusEnum.AWAIT.toString()) //
                .addFilter("worker", ":", "worker2"), //
                QueueMessageBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(5L)), //
                        hasProperty("totalResults", equalTo(1L)), //
                        hasProperty("content", hasSize(1)) //
                ) //
        );

        assertThat(actual.getContent().get(0), hasProperty("id", equalTo(1009L)));
    }

    @Test
    public void testSearchByWorkerEmpty() {
        final SearchResult<QueueMessageBean> actual = this.service.search(new SearchQuery(0L, 5L) //
                .addFilter("queueCode", MessageQueueStatusEnum.AWAIT.toString()) //
                .addFilter("worker", ":", ""), //
                QueueMessageBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(5L)), //
                        hasProperty("totalResults", equalTo(1L)), //
                        hasProperty("content", hasSize(1)) //
                ) //
        );

        assertThat(actual.getContent().get(0), hasProperty("id", equalTo(1001L)));
    }

    @Test
    public void testSearchByWorkerUnknown() {
        final SearchResult<QueueMessageBean> actual = this.service.search(new SearchQuery(0L, 5L) //
                .addFilter("queueCode", MessageQueueStatusEnum.AWAIT.toString()) //
                .addFilter("worker", "!", ""), //
                QueueMessageBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(5L)), //
                        hasProperty("totalResults", equalTo(2L)), //
                        hasProperty("content", hasSize(2)) //
                ) //
        );

        assertThat(actual.getContent().get(0), hasProperty("id", equalTo(1002L)));
    }

    @Test
    public void testMove() {

        final QueueMessageBean actual = this.service.findByUid("2017-01-ANA-RJL-50");
        final Date date = new Date();
        this.service.move(actual.getId(), MessageQueueStatusEnum.SUCCESS.toString(), date);

        final QueueMessageBean afterMove = this.service.findByUid("2017-01-ANA-RJL-50");

        assertThat(afterMove, allOf( //
                Arrays.asList( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("updated", notNullValue()), //
                        hasProperty("queueCode", equalTo(MessageQueueStatusEnum.SUCCESS.toString())), //
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-50"))) //
        ) //
        );

    }

    @Test
    public void testDeleteById() {

        final QueueMessageBean actual = this.service.findByUid("2017-01-ANA-RJL-50");
        this.service.deleteById(actual.getId());

        final QueueMessageBean afterDelete = this.service.findByUid("2017-01-ANA-RJL-50");

        final QueueMessageFileBean file = this.serviceFile.findByIdMessage(actual.getId());

        assertThat(afterDelete, nullValue());
        assertThat(file, nullValue());

    }

    @Test
    public void testDeleteByUid() {

        final QueueMessageBean actual = this.service.findByUid("2017-01-ANA-RJL-50");
        this.service.deleteByUid(actual.getUid());

        final QueueMessageBean afterDelete = this.service.findByUid("2017-01-ANA-RJL-50");

        final QueueMessageFileBean file = this.serviceFile.findByIdMessage(actual.getId());

        assertThat(afterDelete, nullValue());
        assertThat(file, nullValue());

    }

    @Test
    public void testDeleteNotExistingQueue() {

        assertEquals(this.service.deleteByUid("2017-01-ANA-RJL-57"), 0L);

    }

}
