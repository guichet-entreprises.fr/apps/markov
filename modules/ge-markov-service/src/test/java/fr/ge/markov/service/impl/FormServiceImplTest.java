/**
 *
 */
package fr.ge.markov.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.markov.service.IFormsService;
import fr.ge.markov.service.data.AbstractTest;

/**
 * @author bsadil
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class FormServiceImplTest extends AbstractTest {

    @Autowired
    private IFormsService formService;

    @Autowired
    private Engine engine;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHandleStepNominal() throws Exception {
        final byte[] file = this.resourceAsBytes("nominal.zip");
        final IProvider provider = new ZipProvider(file);
        final SpecificationLoader loader = this.engine.loader(provider);
        final int beforeStepIndex = loader.stepsMgr().current().getPosition();

        final boolean result = this.formService.handleStep(provider);

        assertEquals(false, result);
        assertEquals(beforeStepIndex, loader.stepsMgr().current().getPosition());
    }

    @Test
    public void testHandleStepOtherUser() throws Exception {
        final byte[] file = this.resourceAsBytes("other-user.zip");
        final IProvider provider = new ZipProvider(file);
        final SpecificationLoader loader = this.engine.loader(provider);
        final int beforeStepIndex = loader.stepsMgr().current().getPosition();

        final boolean result = this.formService.handleStep(provider);

        assertEquals(false, result);
        assertEquals(beforeStepIndex, loader.stepsMgr().current().getPosition());
    }

    @Test
    public void testHandleStepLast() throws Exception {
        final byte[] file = this.resourceAsBytes("last.zip");
        final IProvider provider = new ZipProvider(file);
        final SpecificationLoader loader = this.engine.loader(provider);
        final int beforeStepIndex = loader.stepsMgr().current().getPosition();

        final boolean result = this.formService.handleStep(provider);

        assertEquals(false, result);
        assertEquals(beforeStepIndex, loader.stepsMgr().current().getPosition());
    }

    @Test
    public void testHandleStepNoMore() throws Exception {
        final byte[] file = this.resourceAsBytes("no-more.zip");
        final IProvider provider = new ZipProvider(file);
        final SpecificationLoader loader = this.engine.loader(provider);
        final int beforeStepIndex = loader.stepsMgr().current().getPosition();

        final boolean result = this.formService.handleStep(provider);

        assertEquals(false, result);
        assertEquals(beforeStepIndex, loader.stepsMgr().current().getPosition());
    }

}
