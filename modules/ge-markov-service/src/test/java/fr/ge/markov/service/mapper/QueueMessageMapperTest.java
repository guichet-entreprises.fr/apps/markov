/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.markov.service.mapper;
/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.data.AbstractDbTest;

/**
 * Tests {@link IssueMapper}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class QueueMessageMapperTest extends AbstractDbTest {

    /** The issue mapper. */
    @Autowired
    private IQueueMessageMapper mapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    /**
     * Tests {@link IssueMapper#getIssue(Long)}.
     */
    @Test
    public void testFindByStatus() {
        assertThat(this.mapper.findByStatusAndWorker("AWAIT","1"),
                allOf(Arrays.asList( //
                        hasProperty("id", equalTo(100L)), //
                        hasProperty("queueCode", equalTo("AWAIT")), //
                        hasProperty("worker", equalTo("1")), ///
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-50")), //
                        hasProperty("updated", //
                                hasProperty("time", equalTo(1488530521000L)) //
                        ))));
    }

    @Test
    public void testCreate() {
        final QueueMessageBean queue = new QueueMessageBean();
        queue.setQueueCode("AWAIT");
        queue.setUid("2017-01-ANA-RJL-55");
        queue.setCreated(new Date());
        queue.setUpdated(new Date());
        queue.setWorker("1");

        this.mapper.create(queue);
    }

    @Test
    public void testUpdate() {
        QueueMessageBean queue = new QueueMessageBean();

        queue.setId(100L);
        queue.setQueueCode("SUCCESS");
        queue.setUpdated(new Date());

        this.mapper.update(queue);

        assertThat(this.mapper.findByStatus("SUCCESS"),
                allOf(Arrays.asList( //
                        hasProperty("id", equalTo(100L)), //
                        hasProperty("queueCode", equalTo("SUCCESS")), //
                        hasProperty("worker", nullValue()), ///
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-50")) //
                )));
    }

    @Test
    public void testFindAllQueueByStatus() {
        assertEquals(this.mapper.findAllQueueByStatus("AWAIT").size(), 2);
        assertThat(this.mapper.findAllQueueByStatus("AWAIT").get(0),
                allOf(Arrays.asList( //
                        hasProperty("id", equalTo(100L)), //
                        hasProperty("queueCode", equalTo("AWAIT")), //
                        hasProperty("worker", equalTo("1")), ///
                        hasProperty("uid", equalTo("2017-01-ANA-RJL-50")), //
                        hasProperty("updated", //
                                hasProperty("time", equalTo(1488530521000L)) //
                        ))));
    }

}
