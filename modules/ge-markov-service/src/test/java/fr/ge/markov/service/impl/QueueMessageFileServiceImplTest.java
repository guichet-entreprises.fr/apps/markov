/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.markov.service.IQueueMessageFileService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.service.data.AbstractDbTest;

/**
 * @author Christian Cougourdan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class QueueMessageFileServiceImplTest extends AbstractDbTest {

    @Autowired
    private IQueueMessageFileService service;

    @Before
    public void initDb() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    @Test
    public void testfindIdMessage() {
        final QueueMessageBean queueMessage = new QueueMessageBean();
        queueMessage.setId(1001L);
        final QueueMessageFileBean actual = this.service.findByIdMessage(queueMessage.getId());

        assertThat(actual,
                allOf( //
                        Arrays.asList( //
                                hasProperty("id", equalTo(100L)), //
                                hasProperty("updated", notNullValue()), //
                                hasProperty("created", notNullValue())) //
                ) //
        );
    }

    @Test
    public void testUpdate() {
        final Date now = new Date();

        final QueueMessageBean messageEntity = new QueueMessageBean();
        messageEntity.setId(1002L);
        messageEntity.setQueueCode(MessageQueueStatusEnum.AWAIT.toString());
        messageEntity.setCreated(now);
        messageEntity.setUpdated(now);
        messageEntity.setUid("2017-01-ANA-RJL-99");

        final QueueMessageFileBean fileEntity = new QueueMessageFileBean();
        fileEntity.setContent("Nothing".getBytes(Charset.forName("UTF-8")));
        fileEntity.setCreated(now);
        fileEntity.setUpdated(now);

        this.service.update(fileEntity, messageEntity);

    }

}
