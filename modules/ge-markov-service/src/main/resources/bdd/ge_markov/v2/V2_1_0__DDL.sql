CREATE TABLE queueMessage (
     id         BIGINT      NOT NULL,
     queueCode  VARCHAR(50) NOT NULL,
     worker     VARCHAR(50),
     uid        CHAR(18),
     created    TIMESTAMP   NOT NULL,
     updated    TIMESTAMP   NOT NULL
);

ALTER TABLE queueMessage
    ADD CONSTRAINT pk_queueMessage
    UNIQUE (id);

CREATE SEQUENCE sq_queueMessage
    START 1
    MINVALUE 1
    CACHE 1;

CREATE TABLE IF NOT EXISTS  queueMessageFile (
    id              BIGINT      NOT NULL,
    queueMessage_id BIGINT      NOT NULL,
    content         bytea        NOT NULL,
    created         TIMESTAMP   NOT NULL,
    updated         TIMESTAMP   NOT NULL
);


ALTER TABLE queueMessageFile
    ADD CONSTRAINT pk_queueMessageFile
    UNIQUE (id);
    
CREATE SEQUENCE sq_queueMessageFile
    START 1
    MINVALUE 1
    CACHE 1;

ALTER TABLE queueMessageFile
    ADD CONSTRAINT fk_queueMessageFile_queueMessage
    FOREIGN KEY (queueMessage_id)
    REFERENCES queueMessage(id) ON DELETE CASCADE;


