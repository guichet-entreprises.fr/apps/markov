CREATE TABLE togglz (
    feature_name character varying(100) NOT NULL,
    feature_enabled integer,
    strategy_id character varying(200),
    strategy_params character varying(2000)
);

ALTER TABLE togglz ADD CONSTRAINT togglz_pkey PRIMARY KEY (feature_name);