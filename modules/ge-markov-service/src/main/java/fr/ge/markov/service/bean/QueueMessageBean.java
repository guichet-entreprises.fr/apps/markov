/**
 *
 */
package fr.ge.markov.service.bean;

/**
 * @author bsadil
 *
 */
public class QueueMessageBean extends DatedBean<QueueMessageBean> {

    /** Status. */
    private String queueCode;

    /**
     * Worker
     */
    private String worker;

    /**
     * uid
     */
    private String uid;

    /**
     * @return the worker
     */
    public String getWorker() {
        return this.worker;
    }

    /**
     * @param worker
     *            the worker to set
     */
    public QueueMessageBean setWorker(final String worker) {
        this.worker = worker;
        return this;
    }

    /**
     * @return the queueCode
     */
    public String getQueueCode() {
        return this.queueCode;
    }

    /**
     * @param queueCode
     *            the queueCode to set
     */
    public QueueMessageBean setQueueCode(final String queueCode) {
        this.queueCode = queueCode;
        return this;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public QueueMessageBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

}
