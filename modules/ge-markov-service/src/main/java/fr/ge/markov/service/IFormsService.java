/**
 *
 */
package fr.ge.markov.service;

import fr.ge.common.nash.engine.provider.IProvider;

/**
 * Forms service.
 * 
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */
public interface IFormsService {

    /**
     * Markov execution for handling one step execution.
     *
     * @param provider
     *            the provider
     * @return true if next step available
     */
    boolean handleStep(IProvider provider);

}
