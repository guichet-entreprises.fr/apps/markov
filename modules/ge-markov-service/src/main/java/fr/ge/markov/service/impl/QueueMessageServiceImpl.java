/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.service.IQueueMessageService;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.service.bean.QueueMessageSummaryBean;
import fr.ge.markov.service.mapper.IQueueMessageFileMapper;
import fr.ge.markov.service.mapper.IQueueMessageMapper;
import fr.ge.markov.service.persistence.entity.QueueMessageSummaryEntity;

/**
 * @author Christian Cougourdan
 *
 */
@Service("queueMessageService")
public class QueueMessageServiceImpl implements IQueueMessageService {

    @Autowired
    private Mapper dozerMapper;

    @Autowired
    private IQueueMessageMapper queueMessageMapper;

    @Autowired
    private IQueueMessageFileMapper queueMessageFileMapper;

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueMessageServiceImpl.class);
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = true)
    public QueueMessageSummaryBean summary() {
        final List<QueueMessageSummaryEntity> entities = this.queueMessageMapper.totalEntries();

        final QueueMessageSummaryBean summary = new QueueMessageSummaryBean();

        if (null != entities) {
            entities.forEach(entity -> summary.add(entity.getQueueName(), entity.getTotalEntries()));
        }

        return summary;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = true)
    public QueueMessageBean findByStatus(final String queueCode) {
        return this.queueMessageMapper.findByStatus(queueCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public long create(final QueueMessageBean entity) {
        return this.queueMessageMapper.create(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public long update(final QueueMessageBean entity) {
        entity.setUpdated(new Date());
        return this.queueMessageMapper.update(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public QueueMessageBean add(final String queueName, final byte[] data, final String uid) {
        final Date now = new Date();

        final QueueMessageBean messageEntity = new QueueMessageBean();
        messageEntity.setQueueCode(queueName);
        messageEntity.setCreated(now);
        messageEntity.setUpdated(now);
        messageEntity.setUid(uid);

        final QueueMessageFileBean fileEntity = new QueueMessageFileBean();
        fileEntity.setContent(data);
        fileEntity.setCreated(now);
        fileEntity.setUpdated(now);

        this.queueMessageMapper.create(messageEntity);
        this.queueMessageFileMapper.create(fileEntity, messageEntity);

        return messageEntity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public QueueMessageBean next(final String worker, final MessageQueueStatusEnum queueName) {
        // -->Selected Retrieve the first updated queue message based on queue
        // name
        /*final QueueMessageBean queueMessage = this.queueMessageMapper.findByStatus(queueName.toString());

        // -->Assign the current worker to the queue message and update into db
        if (queueMessage != null && StringUtils.isEmpty(queueMessage.getWorker())) {
            queueMessage.setWorker(worker);
            this.queueMessageMapper.update(queueMessage);
            return this.queueMessageMapper.findByStatusAndWorker(queueName.toString(), worker);
        } else if (queueMessage != null && StringUtils.isNotEmpty(queueMessage.getWorker()) && queueMessage.getWorker().equals(worker)) {
            // -->Return the queue message previously selected
            return queueMessage;
        }*/
        QueueMessageBean queueMessage = this.queueMessageMapper.findByStatusAndWorker(queueName.toString(), worker);
        if (queueMessage != null) {
                LOGGER.info("Process awaiting message #{} and worker #{}", queueMessage.getId(), queueMessage.getWorker());
                return queueMessage;
        }
 
           queueMessage = this.queueMessageMapper.findByStatus(queueName.toString()); // ajouter worker = null
           if (queueMessage != null) {
                queueMessage.setWorker(worker);
            this.queueMessageMapper.update(queueMessage);
                
                LOGGER.info("Process awaiting message #{} and worker #{}", queueMessage.getId(), worker);
                // -->Return the updated queue message assigned to this worker
            return queueMessage;
                //return this.queueMessageMapper.findByStatusAndWorker(queueName.toString(), worker);
        }


        // -->No queue message selected
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = true)
    public QueueMessageFileBean findMessageContent(final long id) {
        return this.queueMessageFileMapper.findByIdMessage(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public void release(final QueueMessageBean queueMessage, final QueueMessageFileBean queueFile) {
        queueMessage.setWorker(null);
        this.update(queueMessage, queueFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public void update(final QueueMessageBean queueMessage, final QueueMessageFileBean queueFile) {
        final Date now = new Date();

        queueMessage.setUpdated(now);
        this.queueMessageMapper.update(queueMessage);

        if (null != queueFile) {
            queueFile.setUpdated(now);
            this.queueMessageFileMapper.update(queueFile, queueMessage);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = true)
    public QueueMessageBean findByUid(final String uid) {
        return this.queueMessageMapper.findByUid(uid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = true)
    public QueueMessageBean findById(final long id) {
        return this.queueMessageMapper.findById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = true)
    public List<QueueMessageBean> findAllQueueByStatus(@Param("queueCode") final String queueCode) {
        return this.queueMessageMapper.findAllQueueByStatus(queueCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = true)
    public <R> SearchResult<R> search(final SearchQuery searchQuery, final Class<R> expectedClass) {
        final Map<String, Object> filters = new HashMap<>();
        if (null != searchQuery.getFilters()) {
            searchQuery.getFilters().forEach(filter -> filters.put(filter.getColumn(), filter));
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<QueueMessageBean> entities = this.queueMessageMapper.findAll(filters, rowBounds);

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            searchResult.setContent(entities.stream().map(elm -> this.dozerMapper.map(elm, expectedClass)).collect(Collectors.toList()));
        }

        searchResult.setTotalResults(this.queueMessageMapper.count(filters));

        return searchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public long move(final Long id, final String destQueueCode, final Date date) {
        return this.queueMessageMapper.move(id, destQueueCode, date);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public long deleteById(final Long id) {
        long deleted = this.queueMessageFileMapper.deleteByIdMessage(id);
        if (deleted > 0) {
            return this.queueMessageMapper.deleteById(id);
        }
        return 0L;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public long deleteByUid(final String uid) {
        return this.queueMessageMapper.deleteByUid(uid);
    }

}
