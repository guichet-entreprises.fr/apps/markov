/**
 *
 */
package fr.ge.markov.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.markov.service.IFormsService;

/**
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */
@Service
public class FormsServiceImpl implements IFormsService {

    @Value("${engine.user.type}")
    private String engineUserType;

    @Autowired
    private Engine engine;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_markov", readOnly = false)
    public boolean handleStep(final IProvider provider) {
        final SpecificationLoader record = this.engine.loader(provider);
        return record.executeActiveStep(this.engineUserType);
    }

}
