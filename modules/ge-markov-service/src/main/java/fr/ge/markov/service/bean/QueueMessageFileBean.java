/**
 *
 */
package fr.ge.markov.service.bean;

import java.util.Optional;

/**
 * The Class QueueMessageFileBean.
 *
 * @author bsadil
 */
public class QueueMessageFileBean extends DatedBean<QueueMessageFileBean> {

    /** The content. */
    private byte[] content;

    /**
     * Gets the content.
     *
     * @return the content
     */
    public byte[] getContent() {
        return this.content;
    }

    /**
     * Sets the content.
     *
     * @param content
     *            the content
     * @return the queue message file bean
     */
    public QueueMessageFileBean setContent(final byte[] content) {
        this.content = Optional.ofNullable(content).map(value -> value.clone()).orElse(null);
        return this;
    }

}
