/**
 *
 */
package fr.ge.markov.service.mapper;

import org.apache.ibatis.annotations.Param;

import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;

/**
 * Queue message file mapper.
 * 
 * @author bsadil
 */
public interface IQueueMessageFileMapper {

    /**
     * Find by queue message ID.
     *
     * @param id
     *            the queue message ID
     * @return the QueueMessageFileBean
     */
    QueueMessageFileBean findByIdMessage(@Param("id") long id);

    /**
     * Creates the QueueMessageBean.
     *
     * @param entity
     *            the QueueMessageBean
     * @param message
     *            the message
     * @return the long
     */
    long create(@Param("queueFile") QueueMessageFileBean entity, @Param("queue") QueueMessageBean message);

    /**
     * Update the QueueMessageBean..
     *
     * @param entity
     *            the QueueMessageBean
     * @param message
     *            the message
     * @return the long
     */
    long update(@Param("queueFile") QueueMessageFileBean entity, @Param("queue") QueueMessageBean message);

    /**
     * Delete by queue message ID.
     * 
     * @param id
     *            the queue message ID
     * @return the long
     */
    long deleteByIdMessage(@Param("id") long id);

}
