package fr.ge.markov.service.bean;

/**
 * Enum StepStatusEnum.
 *
 * @author bsadil
 */
public enum MessageQueueStatusEnum {

    /** in progress. */
    SUCCESS("SUCCESS"),

    /** done. */
    ERROR("ERROR"),

    /** to do. */
    AWAIT("AWAIT"),

    /** inferno. */
    INFERNO("INFERNO");

    /** status. */
    private String status;

    /**
     * Instantie un nouveau step status enum.
     *
     * @param status
     *            status
     */
    MessageQueueStatusEnum(final String status) {
        this.status = status;
    }

    /**
     * Getter on attribute {@link #status}.
     *
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter on attribute {@link #status}.
     *
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

}
