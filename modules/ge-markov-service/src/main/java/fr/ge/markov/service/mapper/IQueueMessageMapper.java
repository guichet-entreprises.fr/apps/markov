/**
 *
 */
package fr.ge.markov.service.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.persistence.entity.QueueMessageSummaryEntity;

/**
 * @author bsadil
 *
 */
public interface IQueueMessageMapper {

    /**
     * Find by Status of queue (exp: Await or Error or Success).
     *
     * @param queueCode
     *            the queueCode
     * @return the QueueMessageBean
     */
    QueueMessageBean findByStatus(@Param("queueCode") String queueCode);

    /**
     * Find QueueMessage by record uid
     *
     * @param uid
     * @return QueueMessage
     */
    QueueMessageBean findByUid(@Param("uid") String uid);

    /**
     * Find queue message by ID
     *
     * @param id
     *            queue message ID
     * @return queue message bean
     */
    QueueMessageBean findById(@Param("id") long id);

    /**
     * Find by Status of queue (exp: Await or Error or Success).
     *
     * @param queueCode
     *            the queueCode
     * @param worker
     *            the worker
     * @return the QueueMessageBean
     */
    QueueMessageBean findByStatusAndWorker(@Param("queueCode") String queueCode, @Param("worker") String worker);

    /**
     * Creates the QueueMessageBean.
     *
     * @param entity
     *            the QueueMessageBean
     * @return the long
     */
    long create(@Param("queue") QueueMessageBean entity);

    /**
     * Update the QueueMessageBean..
     *
     * @param entity
     *            the QueueMessageBean
     * @return the long
     */
    long update(@Param("queue") QueueMessageBean entity);

    /**
     * @param entity
     * @return
     */
    long saveOrUpdate(@Param("queue") QueueMessageBean entity);

    /**
     * move queue to another file (Exp : move queue from Await to success)
     *
     * @param id
     *            of queue
     * @param destQueueCode
     * @param date
     * @return
     */
    long move(@Param("id") Long id, @Param("destQueueCode") String destQueueCode, @Param("now") Date date);

    /**
     * Retrieve total entries per queue.
     *
     * @return total entries by queue name
     */
    List<QueueMessageSummaryEntity> totalEntries();

    /**
     * Retirieve all queue with specific queueCode Exp:
     * queueCode(AWAIT,SUCCESS,ERROR)
     *
     * @param queueCode
     * @return
     */
    List<QueueMessageBean> findAllQueueByStatus(@Param("queueCode") String queueCode);

    /**
     * Search for queue messages.
     *
     * @param filters
     *            filters
     * @param rowBounds
     *            pagination
     * @return queue messages list
     */
    List<QueueMessageBean> findAll(@Param("filters") Map<String, Object> filters, RowBounds rowBounds);

    /**
     * Count queue messages.
     *
     * @param filters
     *            filters
     * @return total elements corresponding
     */
    long count(@Param("filters") Map<String, Object> filters);

    /**
     * delete queue
     *
     * @param id
     *            of queue
     * @return
     */
    long deleteById(@Param("id") Long id);

    /**
     * delete queue
     *
     * @param uid
     *            of queue
     * @return
     */
    long deleteByUid(@Param("uid") String uid);

}
