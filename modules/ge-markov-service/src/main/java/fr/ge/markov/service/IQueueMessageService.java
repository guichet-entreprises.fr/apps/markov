/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.markov.service;

import java.util.Date;
import java.util.List;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.markov.service.bean.MessageQueueStatusEnum;
import fr.ge.markov.service.bean.QueueMessageBean;
import fr.ge.markov.service.bean.QueueMessageFileBean;
import fr.ge.markov.service.bean.QueueMessageSummaryBean;

/**
 * Queue message service.
 *
 * @author Christian Cougourdan
 */
public interface IQueueMessageService {

    /**
     * Retrieve message queues summary, ie total entries per queue.
     *
     * @return message queues summary
     */
    QueueMessageSummaryBean summary();

    /**
     * Find by Status of queue (exp: Await or Error or Success).
     *
     * @param queueCode
     *            the queueCode
     * @return the QueueMessageBean
     */
    QueueMessageBean findByStatus(String queueCode);

    /**
     * Find QueueMessage by record uid.
     *
     * @param uid
     *            the uid
     * @return QueueMessage
     */
    QueueMessageBean findByUid(String uid);

    /**
     * Find queue message by ID.
     *
     * @param id
     *            queue message ID
     * @return queue message bean
     */
    QueueMessageBean findById(long id);

    /**
     * Creates the QueueMessageBean.
     *
     * @param entity
     *            the QueueMessageBean
     * @return the long
     */
    long create(QueueMessageBean entity);

    /**
     * Update the QueueMessageBean..
     *
     * @param entity
     *            the QueueMessageBean
     * @return the long
     */
    long update(QueueMessageBean entity);

    /**
     * A message data to specific queue.
     *
     * @param queueName
     *            queue name
     * @param data
     *            message binary data
     * @param uid
     *            uid
     * @return queue message bean
     */
    QueueMessageBean add(String queueName, byte[] data, String uid);

    /**
     * Finds next queue message.
     *
     * @param worker
     *            the worker
     * @param queueName
     *            the queue name
     * @return the queue message
     */
    QueueMessageBean next(String worker, MessageQueueStatusEnum queueName);

    /**
     * Finds a message content.
     *
     * @param id
     *            the id
     * @return the message content
     */
    QueueMessageFileBean findMessageContent(long id);

    /**
     * Releases a queue message.
     *
     * @param queueMessage
     *            the queue message
     * @param queueFile
     *            the queue file
     */
    void release(QueueMessageBean queueMessage, QueueMessageFileBean queueFile);

    /**
     * Update queue message and queue message file.
     *
     * @param queueMessage
     *            the queue message
     * @param queueFile
     *            the queue file
     */
    void update(QueueMessageBean queueMessage, QueueMessageFileBean queueFile);

    /**
     * Finds all queue messages by status.
     *
     * @param string
     *            the status
     * @return the queue messages
     */
    List<QueueMessageBean> findAllQueueByStatus(String string);

    /**
     * Search for queue messages.
     *
     * @param <R>
     *            the generic type
     * @param query
     *            search parameters
     * @param expectedClass
     *            the expected class
     * @return search result
     */
    <R> SearchResult<R> search(SearchQuery query, Class<R> expectedClass);

    /**
     * Moves queue to another file (Exp : move queue from Await to success).
     *
     * @param id
     *            of queue
     * @param destQueueCode
     *            the destination queue code
     * @param date
     *            the date
     * @return a long
     */
    long move(Long id, String destQueueCode, Date date);

    /**
     * Deletes queue.
     *
     * @param id
     *            of queue
     * @return a long
     */
    long deleteById(Long id);

    /**
     * Deletes queue.
     *
     * @param uid
     *            the uid
     * @return a long
     */
    long deleteByUid(String uid);

}
